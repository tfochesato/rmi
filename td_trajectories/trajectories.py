#!/usr/bin/env python3

import numpy as np
import json
import math
import argparse
from abc import ABC, abstractmethod
import traceback

import control as ctrl

"""
    Start : ça sert à quoi? Si on prend t < start, c'est censé donner quoi?
    RIZVOLUTION
    Demander si on doit partir d'une vélocité nulle ou pas ?
"""

def buildTrajectory(type_name, start, knots, parameters = None):
    if type_name == "ConstantSpline":
        return ConstantSpline(knots, start)
    if type_name == "LinearSpline":
        return LinearSpline(knots, start)
    if type_name == "CubicZeroDerivativeSpline":
        return CubicZeroDerivativeSpline(knots, start)
    if type_name == "CubicWideStencilSpline":
        return CubicWideStencilSpline(knots, start)
    if type_name == "CubicCustomDerivativeSpline":
        return CubicCustomDerivativeSpline(knots, start)
    if type_name == "NaturalCubicSpline":
        return NaturalCubicSpline(knots, start)
    if type_name == "PeriodicCubicSpline":
        return PeriodicCubicSpline(knots, start)
    if type_name == "TrapezoidalVelocity":
        if parameters is None:
            raise RuntimeError("Parameters can't be None for TrapezoidalVelocity")
        return TrapezoidalVelocity(knots, parameters["vel_max"], parameters["acc_max"], start)
    raise RuntimeError("Unknown type: {:}".format(type_name))

def buildTrajectoryFromDictionary(dic):
    return buildTrajectory(dic["type_name"], dic["start"], np.array(dic["knots"]), dic.get("parameters"))

def buildRobotTrajectoryFromDictionary(dic):
    model = ctrl.getRobotModel(dic["model_name"])
    return RobotTrajectory(model, np.array(dic["targets"]), dic["trajectory_type"],
                           dic["target_space"],dic["planification_space"],
                           dic["start"], dic.get("parameters"))

class Trajectory:
    """
    Describe a one dimension trajectory. Provides access to the value
    for any degree of the derivative

    Parameters
    ----------
    start: float
        The time at which the trajectory starts
    end: float or None
        The time at which the trajectory ends, or None if the trajectory never
        ends
    """
    def __init__(self, start = 0):
        """
        The child class is responsible for setting the attribute end, if the
        trajectory is not periodic
        """
        self.start = start
        self.end = None

    @abstractmethod
    def getVal(self, t, d):
        """
        Computes the value of the derivative of order d at time t.

        Notes:
        - If $t$ is before (resp. after) the start (resp. after) of the
         trajectory, returns:
          - The position at the start (resp. end) of the trajectory if d=0
          - 0 for any other value of $d$

        Parameters
        ----------
        t : float
            The time at which the position is requested
        d : int >= 0
            Order of the derivative. 0 to access position, 1 for speed,
            2 for acc, etc...

        Returns
        -------
        x : float
            The value of derivative of degree d at time t.
        """

    def getStart(self):
        return self.start

    def getEnd(self):
        return self.end

class Spline(Trajectory):
    """
    Attributes
    ----------
    knots : np.ndarray shape (N,2+)
        The list of timing for all the N via points:
        - Column 0 represents time points
        - Column 1 represents the position
        - Additional columns might be used to specify other elements
          (e.g derivative)
    coeffs : np.ndarray shape(N-1,K+1)
        A list of n-1 polynomials of degree $K$
    """

    def __init__(self, knots, start = 0):
        super().__init__(start)
        self.knots = np.array(knots)
        self.end = self.knots[-1][0]
        self.updatePolynomials()

    @abstractmethod
    def updatePolynomials(self):
        """
        Updates the polynomials (self.coeffs) based on the knots and the
        interpolation method
        """

    def getDegree(self):
        """
        Returns
        -------
        d : int
            The degree of the polynomials used in this spline
        """
        return self.coeffs.shape[1] - 1

    @classmethod
    def derivative(cls, a, power, d=1):
        """
            :Exemple
            >>> Spline.derivative(2, 1, 1)
            (2, 0)
            >>> Spline.derivative(2, 2, 1)
            (4, 1)
            >>> Spline.derivative(2, 2, 2)
            (4, 0)
            >> Spline.derivative(7, 6, 45)
            (0, 0)
        """
        if d > power:
            return (0, 0)

        if power < 0 or d < 0:
            raise ValueError("Impossible de dériver avec des trucs négatifs")

        if d == 0:
            return (a, power)

        a = (power * a, power - 1) if power > 0 else (0, 0)
        return cls.derivative(*a, d-1)

    def getVal(self, t, d = 0):
        #TODO: tester

        if t < self.start:
            if d == 0:
                # Tant qu'on est avant le start on la position sera égale à la position du point de départ (aucun mouvement)
                t = self.start
            else:
                # Tant qu'on est avant le départ, la vitesse et l'accélération sont nulles
                return 0.0
        
        if t > self.end:
            # Après le dernier point, la position est égale à la position du dernier point
            # La vitesse et l'accélération sont nulles
            return self.knots[-1][1] if d == 0 else 0.0

        if not self.end is None and t > self.end:
            raise ValueError('Houlala le t il est pas bien', self.start, self.end, t)

        index = max((i for i, x in enumerate(self.knots[:, 0]) if x <= t)) if t != self.end else len(self.coeffs) - 1

        poly = self.coeffs[index]
        #TODO: remplacer ça par un moyen propre de virer le ti quand il n'est pas nécessaire.
        # Les splines cubiques(k=4) n'ont pas besoin du ti 
        ti = self.knots[index][0] if self.getDegree() < 3 else 0

        retval = 0.

        for j, elem in enumerate(poly):
            a, power = Spline.derivative(elem, j, d)
            retval += a * (t - ti)**power

        return retval

class ConstantSpline(Spline):

    def updatePolynomials(self):
        self.coeffs = np.array([[knot[1]] for knot in self.knots[:-1]])

class LinearSpline(Spline):

    def updatePolynomials(self):
        self.coeffs = np.zeros((self.knots.shape[0] - 1, 2))
        for i in range(len(self.knots)-1):
            ti = self.knots[i][0]
            ti1 = self.knots[i+1][0]

            xi = self.knots[i][1]
            xi1 = self.knots[i+1][1]

            self.coeffs[i][0] = xi
            self.coeffs[i][1] = (xi1-xi)/(ti1-ti)

class CubicZeroDerivativeSpline(Spline):
    """
    Update polynomials ensuring derivative is 0 at every knot.
    """
    def updatePolynomials(self):
        self.coeffs = np.zeros((self.knots.shape[0] - 1, 4))
        for i in range(len(self.knots)-1):
            ti = self.knots[i][0]
            ti1 = self.knots[i+1][0]
            xi = self.knots[i][1]
            xi1 = self.knots[i+1][1]

            A = np.array([
              [ti**3, ti**2, ti, 1],
              [ti1**3, ti1**2, ti1, 1],
              [3*ti**2, 2*ti, 1, 0],
              [3*ti1**2, 2*ti1, 1, 0]])
            B = np.array([xi, xi1, 0, 0])
            self.coeffs[i] = np.flip(np.linalg.inv(A) @ B)

class CubicWideStencilSpline(Spline):
    """
    Update polynomials based on a larger neighborhood following the method 1
    described in http://www.math.univ-metz.fr/~croisil/M1-0809/2.pdf
    """
    def updatePolynomials(self):
        self.coeffs = np.zeros((self.knots.shape[0] - 1, 4))

        for i in range(1, len(self.knots)-2):
            tim1 = self.knots[i-1][0]
            ti = self.knots[i][0]
            ti1 = self.knots[i+1][0]
            ti2 = self.knots[i+2][0]

            xim1  =self.knots[i-1][1]
            xi = self.knots[i][1]
            xi1 = self.knots[i+1][1]
            xi2 = self.knots[i+2][1]
            A = np.array([
              [ti**3, ti**2, ti, 1],
              [ti1**3, ti1**2, ti1, 1],
              [tim1**3,tim1**2,tim1,1],
              [ti2**3,ti2**2,ti2,1]])
            B = np.array([xi, xi1, xim1, xi2])
            x = np.flip(np.linalg.inv(A) @ B)
            self.coeffs[i] = x
            if i == 1:
                self.coeffs[i-1] = x
            if i == len(self.knots) - 3:
                self.coeffs[i+1] = x

class CubicCustomDerivativeSpline(Spline):
    """
    For this splines, user is requested to specify the velocity at every knot.
    Therefore, knots is of shape (N,3)
    """
    def updatePolynomials(self):
        self.coeffs = np.zeros((self.knots.shape[0] - 1, 4))
        for i in range(len(self.knots)-1):
            ti = self.knots[i][0]
            ti1 = self.knots[i+1][0]
            xi = self.knots[i][1]
            xi1 = self.knots[i+1][1]

            vi = self.knots[i][2]
            vi1 = self.knots[i+1][2]

            A = np.array([
              [ti**3, ti**2, ti, 1],
              [ti1**3, ti1**2, ti1, 1],
              [3*ti**2, 2*ti, 1, 0],
              [3*ti1**2, 2*ti1, 1, 0]])
            B = np.array([xi, xi1, vi, vi1])
            self.coeffs[i] = np.flip(np.linalg.inv(A) @ B)

class NaturalCubicSpline(Spline):
    def updatePolynomials(self):
        self.coeffs = np.zeros((self.knots.shape[0] - 1, 4))

        m = np.zeros((4*self.knots.shape[0], self.knots.shape[0] * 4))
        B = np.zeros((m.shape[1], 1))

        offset = (len(self.knots)-1) * 2
        for i in range(len(self.knots) - 1):
            ti = self.knots[i][0]
            ti1 = self.knots[i+1][0]
            xi = self.knots[i][1]
            xi1 = self.knots[i+1][1]
            m[2*i, i*4:i*4+4] = np.array([ti**3,ti**2,ti,1])
            m[2*i+1,i*4:i*4+4] = np.array([ti1**3,ti1**2,ti1,1])

            m[offset+2*i, i*4:i*4+8] = np.array([2*ti1**2, 2*ti1, 1, 0, -3*ti1**2, -2*ti, -1,0])
            m[offset+2*i+1,i*4:i*4+8] = np.array([6*ti, 2, 0, 0, -6*ti, -2, 0, 0])

            B[2*i:2*i+2, 0] = np.array([xi, xi1])

        t0 = self.knots[i][0]
        td = self.knots[i][-1]#temps du dernier point
        m[-2, 0:2] = np.array([6*t0,2])
        m[-1, -3:-1] = np.array([6*td,2])

        x = np.linalg.inv(m) @ B
        for i in range(len(self.knots)):
            self.coeffs[i]= x[4*i:4*i+4]

# x0 x1 x2 -> (x0 x1) (x1 x2)

class PeriodicCubicSpline(Spline):
    """
    Describe global splines where position, 1st order derivative and second
    derivative are always equal on both sides of a knot. This i
    """
    def updatePolynomials(self):
        raise NotImplementedError()

class TrapezoidalVelocity(Trajectory):
    def __init__(self, knots, vMax, accMax, start):
        raise NotImplementedError()

    def getVal(self, t, d):
        raise NotImplementedError()

class RobotTrajectory:
    """
    Represents a multi-dimensional trajectory for a robot.

    Attributes
    ----------
    model : control.RobotModel
        The model used for the robot
    planification_space : str
        Two space in which trajectories are planified: 'operational' or 'joint'
    trajectories : list(Trajectory)
        One trajectory per dimension of the planification space
    start : float
        The beginning of the trajectory [s]
    end : float
        The end of the trajectory [s]
    """

    supported_spaces = ["operational", "joint"]

    def __init__(self, model, targets, trajectory_type,
                 target_space, planification_space,
                 start = 0, parameters = None):
        """
        model : ctrl.RobotModel
            The model of the robot concerned by this trajectory
        targets : np.ndarray shape(m,n) or shape(m,n+1)
            The multi-dimensional knots for the trajectories. One row concerns one
            target. Each column concern one of the dimension of the target space.
            For trajectories with specified time points (e.g. splines), the first
            column indicates time point.
        target_space : str
            The space in which targets are provided: 'operational' or 'joint'
        trajectory_type : str
            The name of the class to be used with trajectory
        planification_space : str
            The space in which trajectories are defined: 'operational' or 'joint'
        start : float
            The start of the trajectories [s]
        parameters : dictionary or None
            A dictionary containing extra-parameters for trajectories
        """
        raise NotImplementedError

    def getVal(self, t, dim, degree, space):
        """
        Parameters
        ----------
        t : float
            The time at which the value is requested
        dim : int
            The dimension index
        degree : int
            The degree of the derivative requested (0 means position)
        space : str
            The space in which the value is requested: 'operational' or 'joint'

        Returns
        -------
        v : float
            The value of derivative of order degree at time t on dimension dim
            of the chosen space
        """
        return None

    def getOperationalTarget(self, t):
        """
        Returns:
        target : np.ndarray shape(N,)
            The target for the robot in operational space at time t
        """
        return None

    def getJointTarget(self, t):
        """
        Returns:
        target : np.ndarray shape(N,)
            The target for the robot in joint space at time t
        """
        return None

    def getOperationalVelocity(self, t):
        """
        Returns:
        target : np.ndarray shape(N,)
            The theoric velocity of the robot in operational space at time t
        """
        return None

    def getJointVelocity(self, t):
        """
        Returns:
        target : np.ndarray shape(N,)
            The theoric velocity if the robot in joint space at time t
        """
        return None

    def getStart(self):
        return self.start

    def getEnd(self):
        return self.end

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dt", type=float, default=0.02)
    parser.add_argument("--margin", type=float, default=0.2)
    parser.add_argument("--robot", help="Consider robot trajectories and not 1D trajectories",
                        action="store_true")
    parser.add_argument("--degrees",
                        type=lambda s: np.array([int(item) for item in s.split(',')]),
                        default=[0,1,2],
                        help="The degrees of derivative to plot")
    parser.add_argument("trajectories", nargs="+", type=argparse.FileType('r'))
    args = parser.parse_args()
    trajectories = {}
    tmax = 0
    tmin = 10**10
    for t in args.trajectories:
        try:
            if args.robot:
                trajectories[t.name] = buildRobotTrajectoryFromDictionary(json.load(t))
            else:
                trajectories[t.name] = buildTrajectoryFromDictionary(json.load(t))
            tmax = max(tmax, trajectories[t.name].getEnd())
            tmin = min(tmin, trajectories[t.name].getStart())
        except KeyError as e:
            print("Error while building trajectory from file {:}:\n{:}".format(t.name, traceback.format_exc()))
            exit()
    order_names = ["position", "velocity", "acceleration", "jerk"]
    print("source,t,order,variable,value")
    for source_name, trajectory in trajectories.items():
        for t in np.arange(tmin - args.margin, tmax + args.margin, args.dt):
            for degree in args.degrees:
                order_name = order_names[degree]
                if (args.robot):
                    space_dims = {
                        "joint" : trajectory.model.getJointsNames(),
                        "operational" : trajectory.model.getOperationalDimensionNames()
                    }
                    for space, dim_names in space_dims.items():
                        for dim in range(len(dim_names)):
                            v = trajectory.getVal(t, dim, degree, space)
                            if v is not None:
                                print("{:},{:},{:},{:},{:}".format(source_name,t,order_name,dim_names[dim],v))
                else:
                    v = trajectory.getVal(t,degree)
                    print("{:},{:},{:},{:},{:}".format(source_name,t,order_name,"x",v))
