---
title: TP RMI - Trajectoires
author:
- Maxime BOUTHEROUE DESMARAIS
- Thomas FOCHESATO
- Florian GARDES
- Abdelamnine MEHDAOUI
- Luka MORAIZ
date: 25/11/2020
---

# Trajectoire unidimensionnelle

Les différents types de trajectoires utilisent plusieurs méthodes communes dont `getVal` qui permet d'obtenir une valeur (position, vitesse, accélération, ...) en fonction du temps. Nous avons tout d'abord comparé la valeur du paramètre t avec les valeurs *start* et *end* : si ce paramètre est en dehors de ces bornes, alors la vitesse et l'accélération seront nulles (le robot n'est donc pas encore en mouvement). Pour la position, elle sera égale au premier point de contrôle pour un temps inférieur au départ et au dernier point pour un temps supérieur à *end*.

La variable *coeffs* contient une liste de polynome, chacun permettant de relier deux points. Soient les points suivants : `A=(t0, x0) B=(t1, x1) C=(t2, x2)`. Il y aura alors deux polynomes : le premier pour les points A et B et le second pour les points B et C. Un polynome est stocké dans une liste de taille $K + 1$ où K représente le degré des polynomes, chaque indice correspond à la puissance du produit. Soit le polynome `4x^2 + 2x + 8`, sa représentation dans une liste sera la suivante :

| Indice | Produit |
|--------|---------|
| 0 | 8 |
| 1 | 2 |
| 2 | 4 |

Pour les splines linéaires, la variable x sera remplacée par $t - t_i$ alors que pour les splines cubiques, elle sera uniquement remplacée par t. Pour garder une méthode générique pour toutes les splines, nous avons défini la valeur de 0 si le degré du polynome est supérieur à 3. Une spline cubique ayant un degré de 4, $t - t_i$ reviendra à t.

## Spline constante

Pour comprendre la structure du tp, nous étions invités à implémenter la fonction pour créer une spline constante, c'est-à-dire des polynomes de degré 0 qui indiquent la position du point précédent lorsqu'une valeur de t intermédiaire lui est fourni.  
Comme ce type de spline est de degré 0, la vitesse et l'accélération (respectivement dérivées première et seconde) seront nulles.

![Constant spline](rapport/constant_spline.png)

Nous avons vu en cours qu'une trajectoire (définie par des polynomes) peut appartenir à une classe de continuité. La classe $C^0$ 

## Spline linéaire

Une spline linéaire utilise des polynomes de degré 2 entre chaque points.

![Spline linéaire](rapport/linear.png)

Nous pouvons constater que la vitesse n'est pas continue, la trajectoire n'appartient donc pas à une classe de continuité $C^0$. Son accélération est constante donc elle est $C^1$.

## Spline cubique avec dérivées nulles aux points de passages

Ce type de spline cubique possède deux contraintes pour faire en sortie que la vitesse soit nulle à chaque point de contrôle ce qui impliquera à une décélération du robot à l'approche d'un point.

![Cubic zero derivative](rapport/cube_zero_derivative.png)

Nous pouvons constater que la vitesse est continue mais que l'accélération est infinie, la classe de continuité de cette trajectoire est donc $C^0$