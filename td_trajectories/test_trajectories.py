import random
import unittest

import numpy as np

from trajectories import ConstantSpline, LinearSpline


class TestConstantSpline(unittest.TestCase):

    def setUp(self):
        self.knots = [[0, 2], [1, 4], [3, 8]]

    def test_getDegree(self):
        traj = ConstantSpline(self.knots, 0)
        self.assertEqual(0, traj.getDegree())

    def test_getVal(self):
        traj = ConstantSpline(self.knots, 0.0)

        for i in range(len(self.knots) - 1):
            expected = self.knots[i][1]
            a = self.knots[i][0]
            b = self.knots[i + 1][0]

            # On génère 1000 ti aléatoirements
            for j in range(1000):
                t = random.uniform(a, b - 0.01)
                # Pour tout ti compris entre deux points (t0 et t1), xi doit être égal à x0
                self.assertEqual(expected, traj.getVal(t))

                # La vitesse et l'accélération doivent être nulles
                self.assertEqual(0.0, traj.getVal(t, 1))
                self.assertEqual(0.0, traj.getVal(t, 2))


class TestLinearSpline(unittest.TestCase):

    def setUp(self):
        self.knots = [[0, 2], [1, 4], [2, 3], [4, -1]]

    def test_getDegree(self):
        traj = LinearSpline(self.knots)
        self.assertEqual(1, traj.getDegree())

    def test_getVal(self):
        traj = LinearSpline(self.knots)

        for i in range(len(self.knots) - 1):
            a = self.knots[i][0]
            b = self.knots[i + 1][0]

            # On génère 1000 ti aléatoirements
            for j in range(1000):
                t1 = random.uniform(a, b - 0.01)
                t2 = random.uniform(a, b - 0.01)

                if t1 != t2:
                    # Pour ti et tj différents, on doit avoir deux valeurs différentes
                    self.assertNotEqual(traj.getVal(t1), traj.getVal(t2))

                # L'accélération doit être nulle
                self.assertEqual(0.0, traj.getVal(t1, 2))


if __name__ == '__main__':
    import doctest

    trajectories_module = __import__('trajectories')
    test_suite = doctest.DocTestSuite(trajectories_module)
    unittest.TextTestRunner().run(test_suite)
    unittest.main()
