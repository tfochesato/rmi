from control import RRRRobot
from homogeneous_transform import *

import unittest
import numpy.testing
import numpy as np
from math import pi

rtol = 1e-6
atol = 1e-6

class TestRobot(unittest.TestCase):
    """
    Comparaison des matrices calculées avec numpy (code ci-dessous)
    avec des matrices calculées avec excel

    translation_base = translation(np.array([0,0,1.01]))
    translation_bras_1 = translation(np.array([0.02,0.4,0]))
    translation_bras_2 = translation(np.array([-0.02,0.3,0]))
    translation_bout = translation(np.array([0,0.31,0])) #0.15 + 0.32/2
    rot_dof1 = rot_z(dof1)
    rot_dof2 = rot_x(dof2)
    rot_dof3 = rot_x(dof3)
    puis, on multiplie les matrices et on obtient les matrices de transformation :

    pour dof1=pi/4, dof2=pi/4, dof3=pi/4
    NUMPY :
    [[ 7.07106781e-01,-1.11022302e-16,7.07106781e-01,-4.32842712e-01],
     [ 7.07106781e-01,1.11022302e-16,-7.07106781e-01,4.32842712e-01],
     [ 0.00000000e+00,1.00000000e+00,2.22044605e-16,1.53213203e+00],
     [ 0.00000000e+00,0.00000000e+00,0.00000000e+00,1.00000000e+00]]
    EXCEL:
    0,707106781186547	0	0,707106781186547	-0,432842712474619
    0,707106781186547	0	-0,707106781186547	0,432842712474619
    0	1	0	1,53213203435596
    0	0	0	1

    pour dof1=pi/4, dof2=0, dof3=pi/4
    NUMPY :
    [[ 0.70710678,-0.5,0.5,-0.64997475],
     [ 0.70710678,0.5,-0.5,0.64997475],
     [ 0.,0.70710678,0.70710678,1.2292031 ],
     [ 0.,0.,0.,1.]]
    EXCEL:
    0,707106781186547	-0,5	0,5	-0,649974746830583
    0,707106781186547	0,5	-0,5	0,649974746830583
    0	0,707106781186547	0,707106781186547	1,22920310216783
    0	0	0	1

    pour dof1=0, dof2=pi/4, dof3=0
    NUMPY :
    [[ 1.,0.,0.,0.],
     [ 0.,0.70710678,-0.70710678,0.83133514],
     [ 0.,0.70710678,0.70710678,1.44133514],
     [ 0.,0.,0.,1.]]
    EXCEL:
    1	0	0	0
    0	0,707106781186547	-0,707106781186547	0,831335136523794
    0	0,707106781186547	0,707106781186547	1,44133513652379
    0	0	0	1

    """
    def setUp(self):
        self.robot = RRRRobot(urdf="resources/rrr_robot.urdf")

    def test_flat_arm_transform(self):
        angles = np.array([0.0,0.0,0.0])
        received = self.robot.getBaseFromToolTransform(angles)
        """
            Pas de rotations attendues
            Translations attendues:
            base     :   [0,     0,      1.01]
            bras1    :   [0.02,  0.4,    0]
            bras2    :   [-0.02, 0.3,    0]
            endpoint :   [0,     0.31,   0]  < 0.15 (origin) + 0.32/2 (box length / 2)

        """
        expected = np.array([[1.0, 0.0, 0.0, 0.0],
                     [0.0, 1.0, 0.0, 1.01],
                     [0.0, 0.0, 1.0, 1.01],
                     [0.0, 0.0, 0.0, 1.0]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_1(self):
        dof1 = pi/4
        dof2 = pi/4
        dof3 = pi/4

        angles = np.array([dof1,dof2,dof3])
        received = self.robot.getBaseFromToolTransform(angles)

        expected = np.array([[ 7.07106781e-01,-1.11022302e-16,7.07106781e-01,-4.32842712e-01],
             [ 7.07106781e-01,1.11022302e-16,-7.07106781e-01,4.32842712e-01],
             [ 0.00000000e+00,1.00000000e+00,2.22044605e-16,1.53213203e+00],
             [ 0.00000000e+00,0.00000000e+00,0.00000000e+00,1.00000000e+00]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_2(self):
        dof1 = pi/4
        dof2 = 0
        dof3 = pi/4
        angles = np.array([dof1,dof2,dof3])
        received = self.robot.getBaseFromToolTransform(angles)
        expected = np.array([[ 0.70710678,-0.5,0.5,-0.64997475],
             [ 0.70710678,0.5,-0.5,0.64997475],
             [ 0.,0.70710678,0.70710678,1.2292031 ],
             [ 0.,0.,0.,1.]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_3(self):
        dof1 = 0
        dof2 = pi/4
        dof3 = 0

        angles = np.array([dof1,dof2,dof3])
        received = self.robot.getBaseFromToolTransform(angles)

        expected = np.array([[ 1.,0.,0.,0.],
             [ 0.,0.70710678,-0.70710678,0.83133514],
             [ 0.,0.70710678,0.70710678,1.44133514],
             [ 0.,0.,0.,1.]])
        np.testing.assert_allclose(received, expected, rtol, atol)


if __name__ == "__main__":
    unittest.main()
