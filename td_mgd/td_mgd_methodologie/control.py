import json
import math
import numpy as np
from abc import ABC, abstractmethod
from scipy.optimize import minimize
from homogeneous_transform import *

from urdf_tree_parser import *

class RobotModel:
    """
        Cette classe est destinée à être utilisée par des bras robots uniquement.
    """
    def initContent(self, tool_link_name, endpoint_axis):
        """
            Initialise les matrices de transformations
            str tool_link_name

        """
        extremities = self.robot.extremities
        self.transformations, self.articulations = self.robot.getLinkTransformsChain(tool_link_name)
        self.tool_transformation = self.robot.getExtremityTransform(tool_link_name, axis=endpoint_axis)
        self.joint_names = []
        names = []
        for articulation in self.articulations:
            self.joint_names.append(articulation["name"])

        self.last_joints = [None]*self.getNbJoints()
    def getNbJoints(self):
        """
        Returns
        -------
        length : int
            The number of joints for the robot
        """
        return len(self.getJointsNames())

    def getJointsNames(self):
        """
        Returns
        -------
        joint_names : string array
            The list of names given to robot joints
        """
        return self.joint_names

    def getJointsLimits(self):
        """
        Returns
        -------
        np.array
            The values limits for the robot joints, each row is a different
            joint, column 0 is min, column 1 is max
        """
        #il me semble judicieux de limiter chaque articulation à une rotation totale (-pi, pi)
        jointLimits = []
        for articulation in self.articulations:
            limit = articulation["limits"]
            if limit is None:
                limit = [-np.pi, np.pi]
            jointLimits.append(limit)

        return np.array(jointLimits,dtype = np.double)

    @abstractmethod
    def getOperationalDimensionNames(self):
        """
        Returns
        -------
        joint_names : string array
            The list of names of the operational dimensions
        """

    def getBaseFromToolTransform(self, joints):
        """
        Parameters
        ----------
        joints_position : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The transformation matrix from base to tool

        Retourne un truc du style baseTbody1.Tart(dof1).body1Tbody2.Tart(dof2).body2Tbody3.Tart(dof3)
        avec Tart(dofx) la transformation dépendant de la valeur de l'articulation dofx
        """
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras

        if(self.last_joints == joints).all():
            return self.transformation_matrix
        self.last_joints = joints.copy()
        self.transformation_matrix = np.eye(4)

        for i in range(0, len(joints)):
            axis = self.articulations[i]["axis"]
            type = self.articulations[i]["type"]

            joint_transform = None
            if type == "continuous":
                if axis[0] == 1:
                    joint_transform = rot_x(joints[i])
                elif axis[1] == 1:
                    joint_transform = rot_y(joints[i])
                elif axis[2] == 1:
                    joint_transform = rot_z(joints[i])
                else:
                    print("getBaseFromToolTransform:", axis)
                    joint_transform = np.eye(4)
            elif type == "prismatic":
                joint_transform = translation(joints[i]*axis)
            else:
                raise Exception("Not implemented yet...")
            self.transformation_matrix = self.transformation_matrix.dot(self.transformations[i])
            self.transformation_matrix = self.transformation_matrix.dot(joint_transform)

        self.transformation_matrix = self.transformation_matrix.dot(self.tool_transformation)
        return self.transformation_matrix

    @abstractmethod
    def computeMGD(self, joints):
        """
        Parameters
        ----------
        joints_position : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The coordinate of the effectors in the operational space
        """

    @abstractmethod
    def getOperationalDimensionNames(self):
        """
        Parameters
        ----------
        Returns
        -------
        array The names of all dimensions in operational space
        """

class RTRobot(RobotModel):
    """
    Model a robot with a 2 degrees of freedom: 1 rotation and 1 translation

    The operational space of the robot is 2 dimensional because it can only move inside a plane
    """
    def __init__(self, urdf="resources/rt_robot.urdf"):
        self.robot = URDFTreeRobot(urdf)
        # cette constante spécifie l'axe du "bout du bras".
        endpoint_axis = np.array([0,-1,0])
        self.initContent(tool_link_name="body2", endpoint_axis=endpoint_axis)

    def getJointsNames(self):
        return ["q0", "q1"]

    def computeMGD(self, joints):
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        if(self.last_joints != joints).all():
            self.getBaseFromToolTransform(joints)

        base_tool_pos = self.transformation_matrix[:2,3] #x,y
        return base_tool_pos


    def getOperationalDimensionNames(self):
        return ["x","y"]


class RRRRobot(RobotModel):
    """
    Model a robot with 3 degrees of freedom along different axis
    """
    def __init__(self, urdf="resources/rrr_robot.urdf"):

        self.robot = URDFTreeRobot(urdf)
        #cette constante spécifie l'axe du "bout du bras"
        endpoint_axis = np.array([0,1,0])
        self.initContent(tool_link_name="body3", endpoint_axis=endpoint_axis)

    def computeMGD(self, joints):
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        if(self.last_joints != joints).all():
            self.getBaseFromToolTransform(joints)

        base_tool_pos = self.transformation_matrix[:3,3]
        return base_tool_pos

    def getOperationalDimensionNames(self):
        return ["x","y","z"]


class CustomRobot(RobotModel):
    """
    Test model of a 4 dof robot
    """
    def __init__(self, urdf="resources/custom_robot.urdf"):
        self.robot = URDFTreeRobot(urdf)
        #cette constante spécifie l'axe du "bout du bras"
        endpoint_axis = np.array([0,0,1])
        self.initContent(tool_link_name="body4", endpoint_axis=endpoint_axis)

    def computeMGD(self, joints):
        if(self.last_joints != joints).all():
            self.getBaseFromToolTransform(joints)

        base_tool_pos = self.transformation_matrix[:3,3]
        return base_tool_pos

    def getOperationalDimensionNames(self):
        return ["a","b","c","d"]

def getRobotModel(robot_name):
    robot = None
    if robot_name == "rt":
        robot = RTRobot()
    elif robot_name == "rrr":
        robot = RRRRobot()
    else:
        raise RuntimeError("Unknown robot name: '" + robot_name + "'")
    return robot
