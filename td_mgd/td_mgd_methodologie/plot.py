#!/usr/bin/env python3

import argparse
import pandas as pd
import plotly.express as px

parser = argparse.ArgumentParser()
parser.add_argument("input", help="The csv file containing the simulation output")
args = parser.parse_args()

df = pd.read_csv(args.input)

#plotly affichera le nom du graphique de la facon suivante "name=<nom>"
#cette fonction enleve le "name="et ne garde que le nom
def remove_name_equal(dict_):
    dict_["text"] = dict_["text"].replace("name=","")
    return dict_
print(df)
#fig = px.scatter(df, x="t",y="value",color="source",facet_row="name")
fig = px.line(df, x="t",y="value",color="source",facet_row="name").for_each_annotation(remove_name_equal)
fig.update_yaxes(matches=None)
fig.show()
