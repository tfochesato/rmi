import numpy as np
import math
from scipy.spatial.transform import Rotation as R


def rot_x(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around x
    """
    rotation = np.eye(4)
    rotation[1:3,1:3]= [[math.cos(alpha), -math.sin(alpha)],
                    [math.sin(alpha), math.cos(alpha)]]
    return np.array(rotation)


def rot_y(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around y
    """
    rotation = [
        [math.cos(alpha), 0., math.sin(alpha), 0.],
        [0., 1., 0., 0.],
        [-math.sin(alpha), 0, math.cos(alpha), 0.],
        [0., 0., 0., 1.]
    ]
    return np.array(rotation)


def rot_z(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around z
    """
    rotation = np.eye(4)
    rotation[:2,:2]= [[math.cos(alpha), -math.sin(alpha)],
                    [math.sin(alpha), math.cos(alpha)]]
    return np.array(rotation)

def translation(vec):
    """Return the 4x4 homogeneous transform corresponding to a translation of
    vec
    """
    translation = np.eye(4)
    translation[:3, 3] = vec
    return np.array(translation)

def invert_transform(T):
    rot = T[:3,:3]
    trans = T[:3,3]
    bottom = [[0,0,0,1]]

    i_rot = np.transpose(rot)
    i_trans = np.dot(-i_rot,trans)
    inverse = np.eye(4)
    inverse[:3,:3] = i_rot
    inverse[:3,3] = i_trans
    return inverse

def get_quat(T):
    """
    Parameters
    ----------
    T : np.ndarray shape(4,4)
        A 3d homogeneous transformation matrix

    Returns
    -------
    quat : np.ndarray shape(4,)
        a quaternion representing the rotation part of the homogeneous
        transformation matrix
    """
    rotation = R.from_matrix(T[:3,:3])
    return rotation.as_quat()


def rot_rpy(rpy):
    """
        Retourne la matrice de transformation  homogene correspondant
        à une rotation appliquée dans l'ordre suivant :
        roll (x), pitch (y), yaw (z)
        Ce qui revient à :
        Rz * Ry * Rx
    """
    #selon _la documentation_, l'ordre est :
    #roll pitch yaw
    #  x    y    z
    #source : http://wiki.ros.org/urdf/XML/joint#Elements  (origin, rpy)
    #rotation = Rx . Ry . Rz
    roll, pitch, yaw = rpy

    #http://planning.cs.uiuc.edu/node102.html
    #https://stackoverflow.com/questions/50993172/why-roll-pitch-and-yaw-are-applied-in-that-order
    #perform the roll first, then the pitch, then the yaw
    #P' = Yaw * Pitch * Roll * P
    #P' = Yaw * Pitch * (Roll * P)
    #P' = Yaw * (Pitch * P_Roll)
    rotation = rot_z(yaw).dot(rot_y(pitch)).dot(rot_x(roll))
    #TODO: faire une matrice manuellement, ça serait peut être plus rapide?
    return rotation


if __name__ == "__main__":
    unittest.main()
    # T = rotX(0.3).dot(translation(np.array([1, 2, 3])))
    # print("T: ", T)
    # IT = invertTransform(T)
    # print("T: ", T)
    # print("T^-1: ", IT)
    # print("T*IT: ", T.dot(IT))
