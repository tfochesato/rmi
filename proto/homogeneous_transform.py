import numpy as np

from scipy.spatial.transform import Rotation as R

from numpy.linalg import multi_dot
import sympy as sp


def sym_eye():
    return sp.Matrix([
                     [1, 0, 0, 0],
                     [0, 1, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, 1]])


def sym_rot_x(name):
    q = sp.symbols(name)
    return sp.Matrix([
        [1, 0, 0, 0],
        [0, sp.cos(q), -sp.sin(q), 0],
        [0, sp.sin(q), sp.cos(q), 0],
        [0, 0, 0, 1]]), q


def sym_rot_y(name):
    q = sp.symbols(name)
    return sp.Matrix([
        [sp.cos(q), 0, sp.sin(q), 0],
        [0, 1, 0, 0],
        [-sp.sin(q), 0, sp.cos(q), 0],
        [0, 0, 0, 1]]), q


def sym_rot_z(name):
    q = sp.symbols(name)
    return sp.Matrix([
        [sp.cos(q), -sp.sin(q), 0, 0],
        [sp.sin(q), sp.cos(q), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]]), q


def sym_trans(name, axis):
    q = sp.symbols(name)
    x = q if axis[0] == 1 else 0
    y = q if axis[1] == 1 else 0
    z = q if axis[2] == 1 else 0
    return sp.Matrix([
                     [1, 0, 0, x],
                     [0, 1, 0, y],
                     [0, 0, 1, z],
                     [0, 0, 0, 1]]), q
# Partie numpy


def skew(vector):
    return np.array([[0, -vector[2], vector[1]],
                     [vector[2], 0, -vector[0]],
                     [-vector[1], vector[0], 0]])


def rot_x(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around x
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[1, 0, 0, 0],
                     [0, c, -s, 0],
                     [0, s, c, 0],
                     [0, 0, 0, 1]], dtype=np.double)


def rot_y(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around y
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[c, 0, s, 0],
                     [0, 1, 0, 0],
                     [-s, 0, c, 0],
                     [0, 0, 0, 1]], dtype=np.double)


def rot_z(alpha):
    """Return the 4x4 homogeneous transform corresponding to a rotation of
    alpha around z
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[c, -s, 0, 0],
                     [s, c, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, 1]], dtype=np.double)


def translation(vec):
    """Return the 4x4 homogeneous transform corresponding to a translation of
    vec
    """
    return np.array([[1, 0, 0, vec[0]],
                     [0, 1, 0, vec[1]],
                     [0, 0, 1, vec[2]],
                     [0, 0, 0, 1]], dtype=np.double)


def matrix_exp(vec, theta, transform):
    sk = skew(vec)
    rot = np.eye(3) + np.sin(theta)*sk + (1-np.cos(theta))*(sk @ sk)
    trans = -sk @ transform[:-1, 3]
    rot = np.c_[rot, trans]
    rot = np.r_[rot, [[0, 0, 0, 1]]]
    return rot


def d_rot_x(alpha):
    """Return the 4x4 homogeneous transform corresponding to the derivative of a rotation of
    alpha around x
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[0, 0, 0, 0],
                     [0, -s, -c, 0],
                     [0, c, -s, 0],
                     [0, 0, 0, 0]], dtype=np.double)


def d_rot_y(alpha):
    """Return the 4x4 homogeneous transform corresponding to the derivative of a rotation of
    alpha around y
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[-s, 0, c, 0],
                     [0, 0, 0, 0],
                     [-c, 0, -s, 0],
                     [0, 0, 0, 0]], dtype=np.double)


def d_rot_z(alpha):
    """Return the 4x4 homogeneous transform corresponding to the derivative of a rotation of
    alpha around z
    """
    c = np.cos(alpha)
    s = np.sin(alpha)
    return np.array([[-s, -c, 0, 0],
                     [c, -s, 0, 0],
                     [0, 0, 0, 0],
                     [0, 0, 0, 0]], dtype=np.double)


def d_translation(vec):
    """Return the 4x4 homogeneous transform corresponding to the derivative of a translation of
    vec
    """
    v = vec / np.linalg.norm(vec)
    T = np.zeros((4, 4), dtype=np.double)
    T[:3, 3] = v
    return T


def invert_transform(T):
    I = T.copy()
    RI = T[:3, :3].transpose()
    I[:3, :3] = RI
    I[:3, 3] = -RI @ T[:3, 3]
    return I


def get_quat(T):
    """
    Parameters
    ----------
    T : np.ndarray shape(4,4)
        A 3d homogeneous transformation matrix

    Returns
    -------
    quat : np.ndarray shape(4,)
        a quaternion representing the rotation part of the homogeneous
        transformation matrix
    """
    return R.from_dcm(T[:3, :3]).as_quat()


def rot_rpy(rpy):
    """
        Retourne la matrice de transformation  homogene correspondant
        à une rotation appliquée dans l'ordre suivant :
        roll (x), pitch (y), yaw (z)
        Ce qui revient à :
        Rz * Ry * Rx
    """
    # selon _la documentation_, l'ordre est :
    # roll pitch yaw
    #  x    y    z
    # source : http://wiki.ros.org/urdf/XML/joint#Elements  (origin, rpy)
    #rotation = Rx . Ry . Rz
    roll, pitch, yaw = rpy

    # http://planning.cs.uiuc.edu/node102.html
    # https://stackoverflow.com/questions/50993172/why-roll-pitch-and-yaw-are-applied-in-that-order
    # perform the roll first, then the pitch, then the yaw
    # P' = Yaw * Pitch * Roll * P
    # P' = Yaw * Pitch * (Roll * P)
    # P' = Yaw * (Pitch * P_Roll)

    # Problématique de rotation extrinsèques vs intrinsèquese ...
    # https://dreamanddead.github.io/2019/04/24/understanding-euler-angles.html

    rotation = multi_dot([rot_z(yaw), rot_y(pitch), rot_x(roll)])
    # TODO: faire une matrice manuellement, ça serait peut être plus rapide?
    return rotation


if __name__ == "__main__":
    unittest.main()
