from tkinter import *
from tkinter import ttk

from mgdtab import MGDWindow
#from mgitab import tab_mgi
#from trajtab import tab_trajectories
#from controltab import tab_control

window = Tk()
window.title("URDF Helper")
window.wm_protocol('WM_DELETE_WINDOW', window.quit)

tabControl = ttk.Notebook(window)

#Fabrication des tabs

tab_mgd = ttk.Frame(tabControl)
tab_mgi = ttk.Frame(tabControl)
tab_trajectories = ttk.Frame(tabControl)
tab_control = ttk.Frame(tabControl)

mgd_window = MGDWindow(tab_mgd)

tabControl.add(tab_mgd, text='MGD')
tabControl.add(tab_mgi, text='MGI')
tabControl.add(tab_trajectories, text='Trajectories')
tabControl.add(tab_control, text='Control')


tabControl.pack(expand=1, fill="both")

window.mainloop()

