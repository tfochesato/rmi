from tkinter import *
from tkinter import filedialog as fd
from tkinter import ttk

import networkx as nx
from urdf_tree_parser import URDFExport
import matplotlib.pyplot as plt

import matplotlib.backends.backend_tkagg as tkagg
import math


#Partie métier
class MGDWindow:
    def __init__(self, tab):
        self.tab = tab
        ttk.Label(self.tab, text = "Direct geometric model").grid(column = 0, row = 0,padx = 30,pady = 30)
        self.btn_choose_file = Button(self.tab, text="Urdf path", command=self.onChooseFile)
        self.btn_choose_file.grid(column = 0, row = 1,padx = 30)

        self.btn_update = Button(self.tab, text="update", command=self.update)
        self.btn_update.grid(column = 1, row = 1)
        self.urdfpath = ""
        self.size = (5*100,5*100)
        self.f = plt.figure(figsize=(8,8), dpi=100)
        self.graph = nx.Graph()
        self.canvas = tkagg.FigureCanvasTkAgg(self.f, master=self.tab)
        self.canvas.get_tk_widget().grid(column = 3, row=1,padx = 30)

        self.canvas.callbacks.connect('button_press_event', self.onCanvasClick)
        self.pos = {}
        # a list of nodes:
        nx.draw(self.graph)

        self.roboturdf = None
    
    def onCanvasClick(self,event):
        x = event.xdata
        y = event.ydata
        for name, coord in self.pos.items():
            xx = coord[0]
            yy = coord[1]
            if abs(x-xx) < 0.2 and abs(y-yy) < 0.2:
                self.onSelectJoint(name)
                break
    def onSelectJoint(self, jointname):
        print("selected:",jointname)
    def onChooseFile(self):
        self.urdfpath = fd.askopenfilename()
        self.update()

    def update(self):
        if self.urdfpath is None or self.urdfpath == "":
            return
        self.roboturdf = URDFExport(self.urdfpath)

        self.graph.clear()
        updated_graph = self.roboturdf.getGraph()
        for element in updated_graph:
            self.graph.add_edge(element["parent"], element["child"], name=element["name"])
        self.pos = nx.spring_layout(self.graph)
        nx.draw(self.graph,self.pos, node_size=500,with_labels=True)

        edge_labels = nx.get_edge_attributes(self.graph,'name')
        nx.draw_networkx_edge_labels(self.graph,edge_labels=edge_labels,font_size=8, pos=self.pos , rotate=False)
        self.canvas.draw()
