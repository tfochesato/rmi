--robot
pendulum
--trajectory
robot_trajectories/pendulum_joint_trapezoidal.json
--control-mode
effort
--controller
controllers/pendulum_effort_feed_forward.json
--delay
2
--duration
5
--log
tmp.csv
