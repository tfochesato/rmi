import json
import math
import numpy as np
from abc import ABC, abstractmethod
from scipy.optimize import minimize

from homogeneous_transform import *

from urdf_tree_parser import URDFTreeRobot

from numpy.linalg import pinv, inv
import timeit
import random

class RobotModel:
    """
        Attributs :
        list(np.array)      transforms          liste des transformations fixes du bras
        list(dict)          articulations       liste des caractéristiques des articulations
        fonction            symbolic            fonction de calcul du mgd générée par sympy
        list(str)           joint_names         liste des noms des articulations
        list(float)         last_joints_mgd     dernière liste de valeurs articulaires utilisées pour le calcul du mgd
        list(float)         last_joints_jac     dernière liste de valeurs articulaires utilisées pour le calcul de la jacobienne

        float               patience_factor     facteur qui spécifie la "patience" du truc de jac inverse. Mieux vaut pas trop y toucher.
        int                 max_retries         quantité d'essais randomisés maximum (pour jac inverse) avant de jeter l'éponge
        float               err_min             erreur en dessous de laquelle on considère l'objectif atteint (pour jac inverse)
    """
    def __init__(self,
    tool_link_name,
    endpoint_axis,
    dimension_orgs,
    dimension_names,
    max_retries=10,
    err_min=0.01,
    max_iterations = 1000,
    patience_factor = 0.0001):
        """
            Initialise les matrices de transformations
            str             tool_link_name
            np.array(int,3) endpoint_axis
            array[]         interesting_dimension   liste des dimensions intéressantes pour le mgd
                                                    exemple: ["x",None, "z"] pour un robot dont seuls x et z  nous intéressent
            float           max_dist_per_tick      Pour les méthodes qui utilisent des dérivées du MGI. Distance max entre la cible et l'outil.
        """
        #erreur de position en dessous de laquelle le bras considère avoir atteint sa cible
        self.patience_factor = patience_factor
        self.err_min = err_min
        self.max_iterations = max_iterations
        extremities = self.robot.extremities
        self.transforms, self.articulations, self.symbolic = self.robot.getLinkTransformsChain(tool_link_name, with_extremity_transform=True, axis=endpoint_axis)
        self.joint_names = []
        for articulation in self.articulations:
            self.joint_names.append(articulation["name"])

        self.last_joints_mgd = [None]*self.getNbJoints()
        self.last_joints_jac = [None]*self.getNbJoints()

        self.computed_transforms = [None]*(len(self.transforms)-1)

        #tableau des dimensions utilisées
        self.dimension_orgs = dimension_orgs
        self.dimension_names = dimension_names

        self.jacobian = np.zeros((len(self.dimension_orgs), len(self.joint_names)), dtype=np.double)
        self.old_val = None
        self.limits = [ articulation["limits"] for articulation in self.articulations ]

        self.max_retries = max_retries
        self.previous_target  = [ None ] * len(dimension_orgs)
        self.previous_joints  = [ 0. ] * self.getNbJoints()

        self.success_last_time = False

        #Opti pour accélerer le calcul de la jacobienne :
        #on sauvegarde les transformations permettant d'atteindre chaque
        #link pour éviter des produits de matrice

        #self.getPaulEquations()


    def selector(self, input_transform):
        """
            Cette fonction récupère les composantes importantes dans une matrice homogene
            et retourne sous forme de liste unidirectionnelle
            in:
                np.array    input_transform
                np.array    self.dimension_orgs [4*4]
                np.array    self.dimension_orgs_names = [i]
        """
        return [input_transform[dim[0], dim[1]] for dim in self.dimension_orgs]
        #on récupère les coordonnées intéressantes
    #nothing_algo = ["COBYLA", "Nelder-Mead"]
    all_algos = ["Nelder-Mead",
                "Powell",
                "CG",
                "BFGS",
                "Newton-CG",    #jacobienne obligatoire
                "L-BFGS-B",
                "TNC",
                "COBYLA",
                "SLSQP" ]#needs hessian of hessian product
    jac_algos = ["CG", "BFGS", "Newton-CG", "L-BFGS-B", "TNC", "SLSQP", "dogleg"]
    bound_algos = ["L-BFGS-B", "TNC", "SLSQP", "Powell", "trust-constr"]

    def autotest(self, id=0, max_dist_per_tick=None):

        self.autotest_transpose_error = 0
        if not self.setAutoTestParameters( id, max_dist_per_tick):
            return None
        csv_title = "data/"+self.autotest_algo
        if self.autotest_jac:
            csv_title += "_jac"
        if self.autotest_bounds:
            csv_title += "_bnd"
        if self.max_dist_per_tick is not None:
            csv_title += "_thr"+str(max_dist_per_tick)
        csv_title +=".csv"
        return csv_title

    def setAutoTestParameters(self, id=0, max_dist_per_tick=None):
        self.autotest_algo = ""
        self.autotest_jac = False
        self.autotest_bounds = False
        self.max_dist_per_tick = max_dist_per_tick
        for algo in RobotModel.all_algos:
            jac = (algo in RobotModel.jac_algos)
            bound = (algo in RobotModel.bound_algos)
            if jac and bound:
                for i in range(0,4):
                    if id == 0:
                        self.autotest_jac = (format(i, '02b')[0]=="1")
                        self.autotest_bounds = (format(i, '02b')[1]=="1")
                        self.autotest_algo = algo
                        return True
                    id -= 1
            elif jac and not bound:
                if id == 0:
                    self.autotest_jac = False
                    self.autotest_bounds = False
                    self.autotest_algo = algo
                    return True
                id -= 1
                if id == 0:
                    self.autotest_jac = True
                    self.autotest_bounds = False
                    self.autotest_algo = algo
                    return True
                id -= 1
            elif bound and not jac:
                if id == 0:
                    self.autotest_jac = False
                    self.autotest_bounds = False
                    self.autotest_algo = algo
                    return True
                id -= 1
                if id == 0:
                    self.autotest_jac = True
                    self.autotest_bounds = False
                    self.autotest_algo = algo
                    return True
        return False

    def getNbJoints(self):
        """
        Returns
        -------
        length : int
            The number of joints for the robot
        """
        return len(self.getJointsNames())

    def getJointsNames(self):
        """
        Returns
        -------
        joint_names : string array
            The list of names given to robot joints
        """
        return self.joint_names

    def getJointsLimits(self):
        """
        Returns
        -------
        np.array
            The values limits for the robot joints, each row is a different
            joint, column 0 is min, column 1 is max
        """
        #il me semble judicieux de limiter chaque articulation à une rotation totale (-pi, pi)
        jointLimits = []
        for articulation in self.articulations:
            limit = articulation["limits"]
            if limit is None:
                limit = [-np.pi, np.pi]
            jointLimits.append(limit)

        return np.array(jointLimits,dtype = np.double)

    def getOperationalDimensionNames(self):
        return self.dimension_names

    def getPaulEquations(self):
        a,b,c,d,e,f,g,h,i,x,y,z = sp.symbols("a b c d e f g h i x y z")
        dimensions = [ sp.symbols(dim) for dim in self.dimension_names if dim is not None]

        base_un = sp.Matrix([[a,d,g,x],
                            [b,e,h,y],
                            [c,f,i,z],
                            [0,0,0,1]])
        Un = []
        rhs = []
        i=0
        for sym in self.symbolic["all_functions"]:
            Un.append(sym*base_un)
            base_un = Un[i]
            prevA = self.symbolic["all_functions"][0].copy()
            for j in range(1, len(self.symbolic["all_functions"])-i):
                prevA = prevA*self.symbolic["all_functions"][j]
            rhs.append(prevA)#insert(0,prevA)
            i+=1

        equations = []
        for i in range(len(rhs)):
            for org in self.dimension_orgs:
                equations.append([Un[i][org[0],org[1]], rhs[i][org[0],org[1]]])

        text = "Equations:"
        for equation in equations:
            text += str(sp.simplify(equation[0]))+"="+str(sp.simplify(equation[1]))+"\n"
            print("===")
            print(sp.solve(equation[0]-equation[1], vars=self.symbolic["var"][0:2]))
        return text

    def getBaseFromToolTransformSym(self, joints,recompute=False):
        """
        Parameters
        ----------
        joints_position : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The transformation matrix from base to tool

        Version symbolique de getBaseFromToolTransform.
        """
        if(self.last_joints_mgd == joints).all() and not recompute:
            return self.transformation_matrix
        self.last_joints_mgd = joints.copy()
        self.transformation_matrix = self.symbolic["transform"](*joints)

        return self.transformation_matrix

    def getBaseFromToolTransform(self, joints, recompute=False):
        """
        Parameters
        ----------
        joints_position : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The transformation matrix from base to tool

        Retourne un truc du style baseTbody1.Tart(dof1).body1Tbody2.Tart(dof2).body2Tbody3.Tart(dof3)
        avec Tart(dofx) la transformation dépendant de la valeur de l'articulation dofx
        """
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        if np.array_equal(self.last_joints_mgd,joints) and not recompute:
            return self.transformation_matrix
        self.last_joints_mgd = joints.copy()
        self.transformation_matrix = np.eye(4)

        for i in range(len(joints)):
            self.transformation_matrix = self.transformation_matrix.dot(self.transforms[i])
            self.transformation_matrix = self.transformation_matrix.dot(
                                    self.articulations[i]["transform"](joints[i]))
            self.computed_transforms[i] = np.array(self.transformation_matrix, copy=True)

        self.transformation_matrix = self.transformation_matrix.dot(
                                    self.transforms[-1])
        return self.transformation_matrix

    def computeMGD(self, joints):
        """
        Parameters
        ----------
        joints_position : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The coordinate of the effectors in the operational space
        """
        return self.selector(self.getBaseFromToolTransform(joints))
    @abstractmethod
    def getOperationalDimensionLimits(self):
        """
        Returns
        -------
        limits : np.array(x,2)
            The values limits for the operational dimensions, each row is a
            different dimension, column 0 is min, column 1 is max
        """

    def computeJacobian(self, joints, recompute=False):
        """
        Parameters
        ----------
        joints : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        np.array
            The jacobian of the robot for given joints values
        """
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        nb_joints = len(joints)
        nb_dims = len(self.dimension_orgs)#TODO:GERER SELF.SELECTOR
        jacobian = np.zeros((nb_dims,nb_joints))
        for deriv_joint in range(nb_joints):
            current_transform_matrix = np.eye(4)
            for joint in range(nb_joints):
                current_transform_matrix = current_transform_matrix.dot(self.transforms[joint])
                if joint == deriv_joint:
                    current_transform_matrix = current_transform_matrix.dot(self.articulations[joint]["derivative_transform"](joints[joint]))
                else:
                    current_transform_matrix = current_transform_matrix.dot(self.articulations[joint]["transform"](joints[joint]))
            current_transform_matrix = current_transform_matrix.dot(self.transforms[-1])
            jacobian[:,deriv_joint]=self.selector(current_transform_matrix)
        return jacobian

    def computeJacobianMath(self, joints, recompute=False):
        """
            Livre plutot bien spong - robot modeling and control
            On veut calculer    Jv, la jacobienne de translation
                                    qui donne x,y,z
                                Jw, la jacobienne de rotation
                                    qui donne wx, wy, wz

            vvv Voir page 123 de spong - robot modeling and control vvv
            Pour n le nombre d'articulations
            =>Calculer Jw :<===============
            Jw = [rho1*z0, rho2*z1, ... rhon*zn-1]
            avec rho_n = 1 si articulation n est rotoide et 0 sinon
            avec zi = 0R_{i-1}*k
            avec 0Ri la rotation du repère n exprimée dans le repère 0 (R(q1,q2,q3,...,qi))
            avec k le vectore unitaire de l'axe : [0,0,1]**T par exemple

            On a bien Jw qui fait trois lignes et n colonnes
            =>Calculer Jv :<================
            Jv_i = 0dMGDn/dq_i
            On a n colonnes, une pour chaque joint
            Pour chaque joint :
                zi = 0R_{i-1} * k
                Si le joint est prismatique :
                    Jv_i = zi-1
                Si le joint est rotoide :
                    Jv_i = zi-1 x (o_n-o_i-1)
                    avec x le PRODUIT SCALAIRE
                    o_n le vecteur de l'extrémité du bras (on peut récupérer ça du MGD)
                    o_i-1 le vecteur de l'articulation en cours (faudrait enregistrer ça quand on build le mgd)
            astuce : comme k vaut [1,0,0], [0,1,0] ou [0,0,1], multiplier une matrice de rotation par k revient a extraire
                    la colonne de cette matrice correspondant à la position du 1.

        """
        """https://www.rosroboticslearning.com/jacobian NECESSAIRE POUR LA PARTIE ROTATION"""
        if np.array_equal(self.last_joints_jac,joints) and not recompute:
            return self.jacobian
        if not(np.array_equal(self.last_joints_mgd,joints) and not recompute):
            self.getBaseFromToolTransform(joints)

        jacobian = np.zeros((6,len(joints)))
        for deriv_joint in range(len(joints)):
            unit_axis = self.articulations[deriv_joint]["axis"]
            current_transform = self.computed_transforms[deriv_joint]
            zi = current_transform[:3,:3].dot(unit_axis)
            if self.articulations[deriv_joint]["type"]=="prismatic":
                jacobian[:3,deriv_joint]=zi
                jacobian[3:6,deriv_joint]=[0,0,0]
            else:
                jacobian[3:6,deriv_joint]=zi
                vec = self.transformation_matrix[:3, 3]-current_transform[:3, 3]
                #skew[i]*vec => on fait la matrice antisymmétrique du vecteur vec
                #car c'est plus rapide (???) que le produit vectoriel (voir speedtest.py)
                jacobian[:3,deriv_joint]= skew(zi).dot(vec)
        return self.jacobian

    @abstractmethod
    def analyticalMGI(self, target):
        """
        Parameters
        ----------
        joints : np.array
            The values of the joints of the robot in joint space

        Returns
        -------
        nb_solutions : int
            The number of solutions for the given target, -1 if there is an
            infinity of solutions
        joint_pos : np.ndarray shape(X,) or None
            One of the joint configuration which allows to reach the provided
            target. If no solution is available, returns None.
        """

    def computeMGI(self, joints, target, method):
        """
        Parameters
        ----------
        joints : np.ndarray shape(X,)
            The current position of joints in angular space
        target : np.ndarray shape(X,)
            The target in operational space
        method : str
            The method used to compute MGI, available choices:
            - analyticalMGI
            - jacobianInverse
            - jacobianTransposed
        """
        if method == "analyticalMGI":
            nb_sols, sol = self.analyticalMGI(target)
            if nb_sols == 0:
                return joints
            return sol
        elif method == "jacobianInverse":
            return self.solveJacInverse(joints, target)
        elif method == "jacobianTransposed":
            return self.solveJacTransposed(joints, target)
        raise RuntimeError("Unknown method: " + method)

    def solveJacInverse(self, joints, target):

        """
            Soient
                G(q) le modèle géométrique direct du robot
                o la cible a atteindre : target
                J(q) la jacobienne obtenue en récupérant self.computeJacobian(joint)

                G(q+e)=G(q)+J(q)e pour e petit
                o-G(q) = J(q)e
                e = J(q)**-1(o-G(q))
            + voir ça https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_b-examples_i-inverse-kinematics.html
        """
        if self.success_last_time and (self.previous_target == target).all() :
            return self.previous_joints

        self.previous_target = target.copy()
        max_evol = 0.1 #valeur magique, pas bien. mais ça marche donc bien.


        #on place current_joints entre là ou on est réellement et là ou on voulait aller précedemment pour diminuer les sursauts
        if self.previous_joints[0] is None:
            current_joints = joints.copy()
        else:
            err_prev = target - self.computeMGD(self.previous_joints)
            err_joints = target - self.computeMGD(joints)#*err_joints/(err_prev+err_joints) *err_prev/(err_prev+err_joints)
            current_joints = (joints+ self.previous_joints)/2

        for i in range(0,self.max_retries):
            previous_err = None
            first_try = True
            while True:
                vec = self.computeMGD(current_joints)
                err = (target-vec)
                err_val = np.linalg.norm(err)
                #Si la position est atteinte, c'est bon
                if err_val < self.err_min:
                    success = True
                    break
                if first_try:
                    first_try = False
                else:
                    #Si on se rapproche trop lentement (== pas) de l'objectif, on abandonne
                    if err_val >= previous_err - self.patience_factor*previous_err:
                        success = False
                        break
                jac = self.computeJacobian(current_joints)
                try:
                    evolution = inv(jac).dot(err)
                except:
                    #en cas de non inversible, on break pour rajouter un peu de hasard
                    break;
                evol_norm = np.linalg.norm(evolution)
                if evol_norm > max_evol:
                    evolution *= max_evol/evol_norm
                current_joints += evolution

                previous_err = err_val
            if success:
                break
            #En cas d'échec, on rajoute une part toujours plus grande de hasard
            current_joints = (1-i/self.max_retries) *current_joints + i/self.max_retries*np.array([random.uniform(limit[0],limit[1]) for limit in self.getJointsLimits()])

        if success:
            self.success_last_time = True
            self.previous_joints = current_joints
            return current_joints

        self.success_last_time = False
        self.previous_joints = joints.copy()
        return joints


    """    jac_algos = ["CG", "BFGS", "Newton-CG", "L-BFGS-B", "TNC", "SLSQP", "trust-constr"]
    """
    def solveJacTransposed(self, joints, target):
        t_id = 0
        vec = self.computeMGD(joints)
        err = (target - vec)

        def generator_function(joints):
            err = target - self.computeMGD(joints)
            jac_transpose = -2*self.computeJacobian(joints).transpose().dot(err)
            return  (np.sum(err**2),jac_transpose)

        default_result = joints
        result = minimize(generator_function,
                            x0=default_result,
                            method="TNC",
                            jac=True)
        if result.success:
            return result.x
        return joints

    def solveJacTransposedTest(self, joints, target):
        t_id = 0
        vec = self.computeMGD(joints)
        err = (target - vec)
        if self.max_dist_per_tick is not None:
            for i in range(len(vec)):
                if err[i] > self.max_dist_per_tick:
                    target[i] = vec[i] + self.max_dist_per_tick
                elif err[i] < -self.max_dist_per_tick:
                    target[i] = vec[i] - self.max_dist_per_tick

        bounds = None
        if self.autotest_bounds :
            bounds = []
            for limit in self.limits:
                if limit is None:
                    bounds.append([-np.pi/2, np.pi/2])
                else:
                    bounds.append(limit)

        def generator_function(joints):
            err = target - self.computeMGD(joints)
            jac_transpose = -2*self.computeJacobian(joints).transpose().dot(err)
            if self.autotest_jac:
                return  (np.sum(err**2),jac_transpose)
            else:
                return np.sum(err**2)

        result = minimize(generator_function,
                            x0=joints,
                            method=self.autotest_algo,
                            bounds=bounds,
                            jac=self.autotest_jac)
        if result.success:
            return result.x
        else:
            self.autotest_transpose_error += 1
            return joints

    def checkLimits(self, joints):
        limits = self.getJointsLimits()
        for i in range(len(limits)):
            if(joints[i]< limits[i][0] or joints[i]>limits[i][1]):
                return False
        return True

class RTRobot(RobotModel):
    """
    Model a robot with a 2 degrees of freedom: 1 rotation and 1 translation

    The operational space of the robot is 2 dimensional because it can only move inside a plane
    """

    def __init__(self, urdf="resources/rt_robot.urdf"):
        self.robot = URDFTreeRobot(urdf)
        # cette constante spécifie l'axe du "bout du bras".
        RobotModel.__init__(self,
                            tool_link_name="body2",
                            endpoint_axis=np.array([0,-1,0]),
                            dimension_orgs=[[0,3],[1,3]],
                            dimension_names=["x","y"],
                            max_retries=5)

    def computeMGD(self, joints):
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        if(self.last_joints_mgd != joints).all():
            self.getBaseFromToolTransform(joints)
        base_tool_pos = self.transformation_matrix[:2,3] #x,y
        return base_tool_pos

    def getOperationalDimensionLimits(self):
        #attention, c'est pas "45" mais : sqrt(25^2+45^2)
        return np.array([[-0.5147,0.5147],[-0.5147,0.5147]])

    def analyticalMGI(self, target):
        d = np.linalg.norm(target)
        x = target[0]
        y = target[1]
        L1 = self.transforms[1][0,3]
        L2 = -(self.transforms[2][1,3]+self.transforms[1][1,3])
        try:
            q2 = math.sqrt(d**2 - L2**2) - L1
        except ValueError:
            return 0, None

        q1 = math.atan2(y, x) + math.atan2(L2, L1 + q2)
        retval = np.array([q1, q2])
        if self.checkLimits(retval):
            return 1, [q1, q2]
        return 0, None
class RRRRobot(RobotModel):
    """
    Model a robot with 3 degrees of freedom along different axis
    """
    def __init__(self, urdf="resources/rrr_robot.urdf"):
        self.robot = URDFTreeRobot(urdf)
        RobotModel.__init__(self,
                            tool_link_name="body3",
                            endpoint_axis=np.array([0,1,0]),
                            dimension_orgs=[[0,3],[1,3],[2,3]],
                            dimension_names=["x","y","z"])
    def computeMGD(self, joints):
        #On veut éviter de recalculer tout le temps
        #Vérifié, ca evite pas mal d'appels surtout quand on bouge pas le bras
        if(self.last_joints_mgd != joints).all():
            self.getBaseFromToolTransform(joints)

        base_tool_pos = self.transformation_matrix[:3,3]
        return base_tool_pos

    def getOperationalDimensionLimits(self):
        return np.array( [[-1.01, 1.01], [-1.01, 1.01],[0.4,1.62]])

    def analyticalMGI(self, target):
        d = np.linalg.norm(target)
        L1 = self.transforms[1][1,3]
        L2 = self.transforms[2][1,3]
        L3 = self.transforms[3][1,3]
        d2 = d - L3

        if d2 > L1 + L2 or d2 < abs(L2 - L1):
            print(d2, L1 + L2)
            return 0, None

        x, y, z = target[0], target[1], target[2]

        angle = math.atan2(y, x) - math.pi / 2

        #q2 = math.acos((L1**2 + d2**2 - L2**2) / (2 * L1 * d2))
        #q3 = math.acos((L1**2 + L2**2 - d2**2) / (2 * L1 * L2))

        translated_and_rotated = self.transforms[0].dot(rot_x(angle)).dot(self.transforms[1]).dot(np.array([0,0,0,1]))
        distance = np.linalg.norm(np.array([target[0],target[1]]) - translated_and_rotated[:2] )
        q3 = math.acos(distance)
        print(distance)

        q2 = math.acos(L1**2 - L2**2 + distance**2)/ (2*L1*L2)
        return 1, np.array([angle, q2, q3])
        if d == L1 + L2:
            # 1 solution pour q1 et q2
            return 1, np.array([angle, q2, q3])
        else:
            # 2 solutions pour q1 et q2
            return 2, np.array([angle, angle - q2, angle + q3])

class LegRobot(RobotModel):
    """
    Model of a simple robot leg with 4 degrees of freedom
    """
    def __init__(self, urdf="resources/leg_robot.urdf"):
        self.robot = URDFTreeRobot(urdf)
        #cette constante spécifie l'axe du "bout du bras"
        RobotModel.__init__(self,
                            tool_link_name="body4",
                            endpoint_axis=np.array([0,1,0]),
                            dimension_orgs=[[0,3],[1,3],[2,3],[2,1]],
                            dimension_names=["x","y","z","r"])


    def getOperationalDimensionLimits(self):
        return np.array( [[-1.21, 1.21], [-1.21, 1.21],[0.4,1.62],[-1, 1]])

    def analyticalMGI(self, target):
        #TODO: implement
        raise NotImplementedError()

def getRobotModel(robot_name):
    robot = None
    if robot_name == "rt":
        robot = RTRobot()
    elif robot_name == "rrr":
        robot = RRRRobot()
    elif robot_name == "leg":
        robot = LegRobot()
    else:
        raise RuntimeError("Unknown robot name: '" + robot_name + "'")
    return robot
