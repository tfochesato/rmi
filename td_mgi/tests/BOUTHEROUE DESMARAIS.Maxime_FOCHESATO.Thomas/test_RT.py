from control import *
from homogeneous_transform import *

from simulation import *
import unittest
import numpy.testing
import numpy as np
from math import pi

rtol = 1e-6
atol = 1e-6
class TestRTRobot(unittest.TestCase):
    def setUp(self):
        self.rt_robot = RTRobot(urdf="resources/rt_robot.urdf")
    def test_RT_operational_names(self):
        np.testing.assert_equal(["x","y"], self.rt_robot.getOperationalDimensionNames())
    #TODO implement other tests for jac inverse
    def test_RT_operational_limits(self):
        expected = np.array([[-0.5147,0.5147],[-0.5147,0.5147]])
        np.testing.assert_equal(expected, self.rt_robot.getOperationalDimensionLimits())

    def test_RT_MGI_analytique_hors_cadre(self):
        target=[0,0]
        expected=0
        returned=self.rt_robot.analyticalMGI(target)[0]
        np.testing.assert_equal(expected, returned)

    def test_RT_MGI_analytique_hors_cadre_2(self):
        target=[0.5,0.5]
        expected=0
        returned=self.rt_robot.analyticalMGI(target)[0]
        np.testing.assert_equal(expected, returned)

    def test_RT_MGI_analytique(self):
        target=  [0.32, 0.32]
        joints = np.array(self.rt_robot.analyticalMGI(target)[1])
        np.testing.assert_allclose(target,self.rt_robot.computeMGD(joints), rtol, atol)


unittest.main()
