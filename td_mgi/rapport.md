---
title: TD Modèle géométrique inverse
author:
- Maxime BOUTHEROUE DESMARAIS
- Thomas FOCHESATO
date: 23/10/2020
---

## Introduction

Le modèle géométrique inverse (MGI) permet de déterminer les valeurs de chaque articulation d'un robot en fonction d'une position. La simulation que nous utilisons permet de spécifier des coordonnées cibles vers lesquelles le robot devra se déplacer. Ce td nous proposait d'implémenter le calcul du MGI à l'aide de trois méthodes différentes pour ensuite les comparer.

## Matrice Jacobienne

Deux méthodes pour le calcul du MGI utilisent une matrice Jacobienne : elle aura été calculée à partir de dérivées partielles des matrices de transformation du robot. Elle dépend des angles articulaires.

## Inverse de la Jacobienne

Cette méthode fait appel à l'inverse de la Jacobienne pour calculer une approximation des valeurs de chaque articulation en utilisant un processus itératif. Au moins une condition d'arrêt est nécessaire pour éviter que l'algorithme s'exécute en boucle : nous avons tout d'abord choisi un nombre maximal d'itérations. Ce paramètre aurait permit d'arrêter l'algorithme en cas d'absence de solution.
Bien que fonctionnel, cela mène à des temps de calculs inconfortables en cas d'approche de singularité ou de blocage. Nous avons donc choisi d'arrêter la recherche en cas d'arrêt de la diminution de l'erreur, permettant ainsi de diminuer drastiquement l'ampleur des ralentissements.
Dans certaines situations, le bras n'arrivait pas à atteindre des positions parfaitement atteignables : pour éviter cela, on rajoute une part de hasard de plus en plus grande à chaque tentative de résolution.
Nous avons également ajouté un paramètre qui concerne la précision souhaitée de la solution : la qualité de l'approximation ainsi que le temps d'exécution de l'algorithme dépendront de ce paramètre.

Comme cette méthode doit calculer l'inverse de la Jacobienne, nous devions traiter les cas ou la matrice n'est pas inversible comme en cas de singularité. Nous avons utilisé la fonction `inv` de numpy pour inverser la matrice : dans le cas où ce n'est pas possible, le programme effectue une tentative en augmentant la part de hasard.

## Transposée de la Jacobienne

Le calcul du MGI à l'aide de la transposée de la Jacobienne nécessite la minimisation d'une fonction de coût d'erreur. L'erreur est définie comme la différence entre la position souhaitée et la position actuelle. Cette méthode peut être optimisée en utilisant le gradient de cette fonction. La bibliothèque scipy propose une fonction `minimize` qui permettra de minimiser une fonction à partir de valeurs initiales en utilisant un algorithme de résolution.

## Résolution géométrique

La dernière méthode fait appel aux propriétés géométriques du robot pour calculer le MGI. Les robots utilisés dans ce td sont n'ont pas les mêmes articulations et n'opèrent pas tous sur les mêmes axes, nous avons dû trouver des solutions pour chacun d'entre eux. La résolution géométrique est utilisée dans des configurations avec un petit nombre d'articulations, là où il est possible de découper le problème en formes géométriques, par exemple un triangle rectangle.

### Robot RT

Le robot RT possède deux articulations : une rotoïde sur l'axe x et une linéaire sur l'axe y. Cette configuration peut être représentée sur un plan 2d : la première articulation (appelée q0) sera la valeur d'un angle et la seconde (appelée q1) sera une distance.

Nous avons tout d'abord commencé par calculer la valeur de q1 en utilisant le théorème de Pythagore. En effet, la situation du robot rt peut être vue comme un triangle rectangle sur un plan 2d. Nous pouvons écrire l'équation suivante : $$D^2 = (q1 + L1)^2 + L2^2$$. Cela revient à dire que $$q1 = \sqrt{D^2 - L2^2} - L1$$

La valeur de q0 peut être calculée à l'aide de deux angles : le premier indiquera l'orientation de la cible et le second un décalage lié à la structure du robot : son extrémité n'étant pas dans l'axe. Cela revient aux formules suivantes :

Connaissant les longueurs des côtés opposé et adjacent, nous pouvons utiliser la formule : $tan(\theta) = \frac{opposé}{adjacent}$

L'angle qui indique un décalage se calcul en utilisant les valeurs L1, L2 et q1 : $$\theta_1 = \arctan{\frac{L2}{L1 + q1}}$$

Voici un schéma qui décrit la configuration :

![](rapport/schema_rt.jpg) { width=50% }


### Robot RRR

non :()

## Robot Leg


## Débogage

Pour faciliter la mise en œuvre des méthodes, nous avons utilisé plusieurs techniques. Nous avons ajouté l'affichage de la position de l'indicateur d'axes dans l'espace opérationnel au dessus de celui-ci : il nous était possible de vérifier si les valeurs calculés pour les joints permettaient au robot d'atteindre sa cible ou non. Cet affichage a été réalisé grâce à la fonction `addUserDebugText` de pybullet.

Nous avons également détourner l'usage principal de l'indicateur d'axe pour qu'il se positionne à l'endroit ciblé : cela nous permettait de comparer la position du robot avec la position à laquelle il aurait dû aller.

Comme demandé dans les consignes, nous avons enrichit notre fichier de logs pour qu'ils contiennent la position de la cible et le temps de calcul à chaque tick pour la méthode utilisée. Comme évoqué précédemment, nous avons cherché à optimiser la vitesse d'exécution de la méthode inverseJacobian. Les résultats de ces optimisations ont pu être constaté grâce au fichier de logs.

## Discussion sur les méthodes

Après avoir implémenté trois méthodes différentes pour calculer le MGI sur différents robots, nous avons pu nous faire un avis sur chacune d'entre elles. La résolution géométrique présente l'intérêt de s'exécuter en temps constant et d'obtenir une solution exacte s'il en existe. Un des défauts est qu'il est nécessaire de connaitre parfaitement la configuration du robot pour effectuer les bons calculs. Les méthodes utilisant la Jacobienne (en excluant le calcul de cette matrice), nous permettent d'utiliser une méthode générale pour tous les robots. Le prix à payer est que une solution (si elle existe) sera une approximation, risque de rencontrer des problèmes à l'approche de singularité et ne peut parfois pas retourner une erreur en cas d'impossibilité du mouvement. Certaines positions atteignables peuvent ne pas être trouvées du fait que l'algorithme ne fait que parcourir un espace non linéaire et risque de se retrouver bloqué par des problèmes locaux. Le temps de calcul est aussi plus important que la résolution géométrique car les deux méthodes ne s'exécutent pas en temps constant, et la qualité de la solution dépendra beaucoup de la qualité de l'algorithme.
