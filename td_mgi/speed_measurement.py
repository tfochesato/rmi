from control import getRobotModel
from homogeneous_transform import *
import timeit
import numpy as np


print("Tests divers et variés de vitessse")

import random
import time

def test1_(robot, j,joints=None):
    if joints is None:
        joints = np.array([random.randint(0,4)]*(j+2))
    robot.getBaseFromToolTransform(joints)
    return robot.computeJacobianSimple(joints)

def test2_(robot, j,joints=None):
    if joints is None:
        joints = np.array([random.randint(0,4)]*(j+2))
    robot.getBaseFromToolTransform(joints)
    return robot.computeJacobian(joints)

a = [1.45,  2.78,   3.785]
b = [4.738, 5.7837, 6.783]

kk=lambda:np.cross(a,b)
kkk=lambda:skew(a).dot(b)
print(kk()-kkk())
print("Produit vectoriel:",timeit.timeit(kk, number=100000))
print("Screw dot:",timeit.timeit(kkk, number=100000))

nb = 1000000
print("Comparaison du MGD par numpy VS MDG issu de sympy")
print("Nombre d'itérations : "+str(nb))
names=  ["rt", "rrr", "leg"]

robot = getRobotModel("rt")
for j in range(len(names)):

    test1 = lambda:test1_(robot,j)
    test2 = lambda:test2_(robot,j)
    print("Robot:"+str(names[j]))
    robot = getRobotModel(names[j])
    a = input("Appuyez sur entrée")
    print("Nombre de links:",robot.getNbJoints())
    print("diff", test1()-test2())
    print("old one:",timeit.timeit(test1, number=10000))
    print("New one:",timeit.timeit(test2, number=10000))
