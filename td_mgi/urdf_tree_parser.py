import xml.etree.ElementTree as ET
from homogeneous_transform import *
from sympy.utilities.lambdify import lambdify, implemented_function
import sympy as sp
# fonctions utilitaires pour trucs d'urdf


def getOrigin(origin_dict=None, xyz_str=None, rpy_str=None):
    """
    Cette fonction prend un origin ou ses composants (venant d'un urdf) et retourne une transformation
    """
    if origin_dict is not None:
        if "xyz" in origin_dict:
            xyz_str = origin_dict["xyz"]
        if "rpy" in origin_dict:
            rpy_str = origin_dict["rpy"]
    # note: xyz et rpy n'apparaitront pas forcément (si nuls, on peut ne pas les spécifier)
    if xyz_str is not None:
        translation_ = list(map(float, xyz_str.split()))
    else:
        translation_ = [0., 0., 0.]
    if rpy_str is not None:
        rpy = list(map(float, rpy_str.split()))
        rotation = rot_rpy(rpy)
    else:
        rotation = np.eye(4)

    transformation = translation(translation_) @ rotation
    return transformation


def getFloatVector(input):
    """
        Cette fonction prend en entrée un str contnenant liste de nombres
        float séparés par des espaces et retourne une liste de floats
    """
    return np.array(list(map(float, input.split())))


def getGeometry(val):
    """
        Prend en entrée un bout d'element tree contenant une géométrie
        et retourne un objet Geometry dûment rempli
    """
    geometry_dict = val.attrib
    name = val.tag
    size, radius, length = None, None, None
    if val.tag == "box":
        size = np.array(getFloatVector(geometry_dict["size"]))
    elif val.tag == "cylinder":
        radius = float(geometry_dict["radius"])
        length = float(geometry_dict["length"])
    elif val.tag == "sphere":
        radius = float(geometry_dict["radius"])
    else:
        print("geometry: "+str(val.tag)+" not implemented yet.")

    return Geometry(name, size=size, radius=radius, length=length)


class Geometry:
    """
        Classe contenant quelques informations de géométrie
        Attributs :
        float       size[3]       Caractéristiques de taille si c'est une box
        str         name          Nom du type d'objet ("box", "cylinder", "sphere" ou "mesh")
        float       radius        Rayon si cylindre ou sphère => name in ("cylinder","sphere")
        float       length        Longueur si cylindre => name == "length"
        str         path          /!\ PAS VRAIMENT IMPLÉMENTÉ /!\ Chemin du fichier mesh si c'est un mesh.
        np.array(3) axis           axe qui permet d'atteindre le bout. Déduit automatiquement si non spécifié (à éviter)
    """

    def __init__(self, name, size, radius, length, path="", axis=None):
        self.size = size
        self.name = name
        self.radius = radius
        self.length = length
        self.path = path
        if axis is not None:
            self.axis = axis
        else:
            self.deduceAxis()

    def deduceAxis(self):
        self.axis = np.array([0, 0, 0])
        if self.name == "box":
            max = np.argmax(self.size)
            self.axis[max] = 1
        elif self.name == "cylinder":
            # length is in Z axis
            self.axis = [0, 0, 1]
        elif self.name == "sphere":
            pass
            # on considère l'axe par défaut d'une sphere comme son centre. C'est arbitraire mais c'est le cas
        elif self.name == "mesh":
            print("extremity not implemented")

    def setAxis(self, axis):
        self.axis = axis

    def getExtremityVector(self, axis=None):
        """
        Retourne un vecteur permettant d'effectuer une translation
        du centre de l'objet à son bord, en suivant l'axe spécifié
        dans axis
        Parametres:
        np.array(3)     axis

        Retourne:
        np.array(3,float)
        """
        if axis is None:
            axis = self.axis
        if self.name == "box":
            return axis * self.size/2
        elif self.name == "cylinder":
            # length is in Z axis
            return axis * np.array([
                self.radius,
                self.radius,
                self.length/2
            ])
        elif self.name == "sphere":
            axis = axis * 1.0/np.sum(axis)  # on normalise
            return axis * self.radius/2
        elif self.name == "mesh":
            print("extremity not implemented")


class RobotJoint:
    """
        Attributs :
            str      name: nom de l'articulation
            str      type: type de l'articulation
            [float,] origin: transformation de l'origine
            [int,] axis: axe de rotation si rotoide, de translation si prismatique
            [float,] limits: tableau contenant la limite supérieure et inférieure de rotation
            --- pas encore utilisé ---
            float    velocity_limit: limite de vitesse de rotation
            float    effort_limit: limite de couple?
    """

    def __init__(self, name, type="", origin=None, axis=None, limits=None, v_limit=0, e_limit=0):
        """
            Paramètres:
            str      name: nom de l'articulation
            str      type: type de l'articulation
            [float,] origin: transformation de l'origine
            [int,] axis: axe de rotation si rotoide, de translation si prismatique
            [float,] limits: tableau contenant la limite supérieure et inférieure de rotation
            --- pas encore utilisé ---
            float    velocity_limit: limite de vitesse de rotation
            float    effort_limit: limite de couple?
        """
        self.name = name
        self.type = type

        self.origin = np.array(origin)
        self.axis = np.array(axis)
        self.limits = limits
        self.velocity_limit = v_limit
        self.effort_limit = e_limit

        self.is_fixed = (self.type == "fixed")

        if self.type == "continuous":
            if self.axis[0] == 1:
                self.transform_function = rot_x
                self.sym_transform, self.sym_var = sym_rot_x(self.name)
                self.derivative_function = d_rot_x
            elif self.axis[1] == 1:
                self.transform_function = rot_y
                self.sym_transform, self.sym_var = sym_rot_y(self.name)
                self.derivative_function = d_rot_y
            elif self.axis[2] == 1:
                self.transform_function = rot_z
                self.sym_transform, self.sym_var = sym_rot_z(self.name)
                self.derivative_function = d_rot_z
            else:
                self.transform_function = lambda x: np.eye(4)
                self.sym_transform = sym_eye()
                self.derivative_function = lambda x: np.zeros((4, 4))
        elif self.type == "prismatic":
            self.sym_transform, self.sym_var = sym_trans(self.name, self.axis)
            self.transform_function = lambda joint: translation(
                joint*self.axis)
            self.derivative_function = lambda joint: d_translation(self.axis)
        else:
            raise Exception(
                "This type of joint is not implemented yet : "+str(self.type))

    @property
    def num_axis_description(self):
        """
        Description de l'axe sous forme de dictionnaire
        """
        return {
            "name": self.name,
            "type": self.type,
            "axis": self.axis,
            "transform": self.transform_function,
            "derivative_transform": self.derivative_function,
            "limits": self.limits,
            "velocity_limit": self.velocity_limit,
            "effort_limit": self.effort_limit
        }

    @property
    def sym_axis_description(self):
        # TODO: add velocity and effort limits
        constraints = []
        if self.limits is not None:
            constraints.append(self.sym_var >= self.limits[0])
            constraints.append(self.sym_var <= self.limits[1])
        return {
            "function": sp.Matrix(self.origin) * self.sym_transform,
            "var": [self.sym_var],
            "constraints": constraints
        }

    def __str__(self):
        return "joint:"+str(self.name)+" axis:"+str(self.axis)


class RobotLink:
    """
        Attributs :
        str                         name
        np.array/transformation     origin
        RobotLink                        parent      Parent du lien
        RobotLink[ ]                     children    Liste des enfants
        dict                        inertial    -> described in __init__
        dict                        visual      -> described in __init__
        dict                        collision   -> described in __init__
    """

    def __init__(self, name):
        """
            Paramètres:
            str name : nom du RobotLink
        """
        self.name = name
        self.originJoint = None
        self.parent = None
        self.children = []

        """
        np.array()/transformation   origin
        float                       mass
        <not implemented>           inertia
        """
        self.inertial = {}

        """
        np.array()/transformation   origin
        Geometry                    geometry
        <not implemented>           material
        """
        self.visual = []

        """
        np.array()/transformation   origin
        Geometry                    geometry
        """
        self.collision = []

    def setParent(self, link, joint):
        """
            Cette fonction définit comme parent du lien self le lien link
            et définit comme enfant du lien link le lien self.
        """
        self.parent = link
        self.originJoint = joint
        link.children.append(self)

    def add_visual(self, visual):
        self.visual.append(visual)

    def add_collision(self, collision):
        self.collision.append(collision)

    @property
    def is_extremity(self):
        """
            Est-ce que ce lien est au bout du graphe?
        """
        return len(self.children) == 0

    @property
    def is_root(self):
        """
            Est-ce que ce lien est au début du graphe?
        """
        return self.parent is None

    @property
    def is_intersection(self):
        """
            Est-ce que ce lien débouche sur plus d'un lien ?
        """
        return len(self.children) > 1

    def getRoot(self):
        """
            Aucun paramètre.
            Retourne la racine du graph depuis n'importe quel point
        """
        return self if self.is_root else self.parent.getRoot()

    def displayTree(self, i=0):
        """
            Aucun paramètre.
            Utile pour la vérification. Fonction récursive qui affiche l'arbre.
        """
        spacing = (i+1)*" "
        if not self.is_root:
            print(spacing+str(self.originJoint))
        print(spacing+str(self))
        for k in range(len(self.children)):
            self.children[k].displayTree(i+1)

    # TODO : renommer
    def getChain(self, transforms=None, joints=None, symbolic=None):
        """
            Aucun paramètre.
            Retourne
            - une liste de matrices de transformations menant à l'origine
                RobotLink duquel cette fonction est appellée, partant de la racine
                Tbase, Tbody1, Tbody2, etc... jusqu'à Tfinal
            - la liste de descripteurs des articulations
            - la fonction symbolique permettant d'atteindre le lien en question
        """
        if self.is_root:
            return transforms, joints, symbolic
        # -------------------------------------------
        # https://docs.python-guide.org/writing/gotchas/
        # https://florimond.dev/blog/articles/2018/08/python-mutable-defaults-are-the-source-of-all-evil/
        # vvv ça, c'est du au probleme des mutable defaults vvv !!!
        if transforms is None:
            transforms = []
            joints = []
            symbolic = self.originJoint.sym_axis_description
            symbolic["all_functions"] = [symbolic["function"].copy()]
        else:
            symbolic["all_functions"].insert(
                0, self.originJoint.sym_axis_description["function"])
            symbolic["function"] = self.originJoint.sym_axis_description["function"] * \
                symbolic["function"]
            symbolic["var"] = self.originJoint.sym_axis_description["var"] + \
                symbolic["var"]
            # garder dans ce sens pour préserver l'ordre des variables
            symbolic["constraints"] += self.originJoint.sym_axis_description["constraints"]

        transforms.insert(0, self.originJoint.origin)
        joints.insert(0, self.originJoint.num_axis_description)
        return self.parent.getChain(transforms=transforms, joints=joints, symbolic=symbolic)

    def getExtremity(self, axis, source="collision"):
        """
            Parametres :
            np.array(3)     axis
            str             source facultatif : peut être "collision" ou "visual"
            Retourne la transformation permettant d'aller de l'origine du RobotLink à son extrémité,
            en suivant le ou les axes spécifiés
        """
        if source == "collision":
            source = self.collision
        elif source == "visual":
            source = self.visual
        else:
            raise Exception("unknown source")

        max_transformation = None
        max_length = 0.0

        for value in source:

            origin = value["origin"]
            if origin is None:
                origin = self.originJoint.origin
            current_extremity = value["geometry"].getExtremityVector(axis)
            # On cherche le vecteur le plus long
            length = np.sum(current_extremity**2)
            if length > max_length:
                max_length = length
                max_transformation = current_extremity

        return origin @ translation(max_transformation)

    def __str__(self):
        val = "link:"+str(self.name)
        val += str(self.inertial)+"\n"
        val += str(self.visual)+"\n"
        val += str(self.collision)+"\n"
        if self.is_root:
            val = "root" + val
        if self.is_extremity:
            val = val + " extremity"
        return val


class URDFExport:
    """
        Cette classe contient les résultats traités du parsing
        de l'urdf, trouve les extrémités et la racine et les matrices
        diverses et variées

        Attributs :
        bool    _up_to_date             False si quand une modification de l'arbre est faite, true après _update
        bool    _is_fully_connected     Est ce qu'il y a plus d'un link sans parent?
        str     _file                   Chemin de l'URDF
        RobotLink    root                    La racine de l'arbre, soit la base du robot. Rempli par _update
        dict(str:RobotLink) _links           La liste des _links. Utile pendant la génération de l'arbre.
                                        key: nom du link, value: le RobotLink
        str[] extremities               Liste des noms des extrémités
    """

    def __init__(self, file):
        """
            Paramètres:
            str     file                        chemin du fichier URDF
            str[]   custom_interesting_link     nom des liens dont on veut calculer les "chaines de transformations"
        """
        self._up_to_date = False
        self._is_fully_connected = False
        self._file = file
        self.root = None
        self._links = {}
        self.extremities = []
        self.parse_urdf()
        self._update()

    def parse_urdf(self):
        urdf_doc = ET.parse(self._file)
        root = urdf_doc.getroot()

        self.name = root.attrib["name"]

        for element_ in root:
            """
                Il existe deux types d'éléments qui nous intéressent :
                les _links, les parties du robot
                les joints, les articulations
            """
            if(element_.tag == "link"):
                """
                    Un link contient au maximum:
                    un nom, le tag "name"
                    des caractéristiques d'inertie "inertial" avec
                        un origine, ex : <origin xyz="0 0 0.5" rpy="0 0 0"/>
                        une masse, ex : <mass value="1"/>
                        une matrice d'inertie, ex : <inertia ixx="1"  ixy="0"  ixz="0" iyy="1" iyz="0" izz="100" />
                    0-n caractéristiques visuelles "visual" avec:
                        un origine, ex : <origin xyz="0 0 0.5" rpy="0 0 0"/>
                        une géométrie, ex : <geometry>
                                                au choix:
                                                    <box size="0 0 0"/>
                                                    <cylinder radius="0" height="0"/>
                                                    <sphere radius="0"/>
                                                    ou
                                                    <mesh filename="/filename/stuff"/>
                                            </geometry>
                        un matériau, mais on s'en fiche actuellement
                    0-n caractéristiques de collision "collision" avec :
                        un origine, ex : <origin xyz="0 0 0.5" rpy="0 0 0"/>
                        une géométrie (structure spécifiée dans "visual")
                """
                name = element_.attrib["name"]

                # On crée (ou récupère) le RobotLink
                link = RobotLink(name)
                if name in self._links:
                    link = self._links[name]

                # On remplit le RobotLink
                inertia = element_.find("inertial")
                if inertia is not None:
                    link.inertial = {
                        "origin": getOrigin(origin_dict=inertia.find("origin").attrib),
                        "mass": float(inertia.find("mass").attrib["value"]),
                        # TODO:générer matrice d'inertie
                        "inertia": inertia.find("inertia").attrib
                    }
                # TODO: a link can have more than one visual
                visuals = element_.findall("visual")
                if visuals is not None:
                    for visual in visuals:
                        origin_found = visual.find("origin")
                        if origin_found is not None:
                            origin = getOrigin(origin_dict=origin_found.attrib)
                        else:
                            origin = None
                        new_visual = {
                            "origin": origin,
                            "geometry": getGeometry(visual.find("geometry").find("*"))
                            # TODO: implement "material":
                        }
                        link.add_visual(new_visual)
                # TODO: a link can have more than one collision
                collisions = element_.findall("collision")
                if collisions is not None:
                    for collision in collisions:
                        origin_found = collision.find("origin")
                        if origin_found is not None:
                            origin = getOrigin(origin_dict=origin_found.attrib)
                        else:
                            origin = None
                        new_collision = {
                            "origin": origin,
                            "geometry": getGeometry(collision.find("geometry").find("*"))
                        }
                        link.add_collision(new_collision)

                # Si le RobotLink est nouveau, on l'ajoute au dict
                if name not in self._links:
                    self._links[name] = link
            elif(element_.tag == "joint"):
                """
                    Exemple de joint, ca parle de soit :
                    TODO: rendre ça plus lisible
                <joint name="dof3" type="continuous">
                    <origin xyz="-0.02 0.3 0" rpy="0 0 0"/>
                    <parent link="body2" />
                    <child link="body3" />
                    <axis xyz="1 0 0" />
                    <limit effort="0" velocity="0" lower="0" upper="0"/>
                    <safety_controller k_position="0" k_velocity="0"
                        soft_lower_limit="0"
                        soft_upper_limit="0"/>
                    <dynamic damping="0">
                    <calibration rising="0">
                </joint>
                """
                # add joint
                name = element_.attrib["name"]
                parent = element_.find("parent").attrib["link"]
                child = element_.find("child").attrib["link"]

                origin = getOrigin(origin_dict=element_.find("origin").attrib)

                axis_dict = element_.find("axis")
                axis = [0, 0, 0]  # None
                if axis_dict is not None:
                    axis = getFloatVector(axis_dict.attrib["xyz"])
                type = element_.attrib["type"]

                limit = None
                v_limit = 0.
                e_limit = 0.
                limit_element = element_.find("limit")
                if limit_element is not None:
                    lower, upper = None, None
                    if "lower" in limit_element.attrib:
                        lower = float(limit_element.attrib["lower"])
                    if "upper" in limit_element.attrib:
                        upper = float(limit_element.attrib["upper"])
                    limit = [lower, upper]
                    if "velocity" in limit_element.attrib:
                        v_limit = float(limit_element.attrib["velocity"])
                    if "effort" in limit_element.attrib:
                        e_limit = float(limit_element.attrib["effort"])

                joint = RobotJoint(name, type=type, origin=origin, axis=axis,
                                   limits=limit, v_limit=v_limit, e_limit=e_limit)
                self.addJoint(parent, child, joint)
            else:
                print("In custom urdf parser : unknown tag :", element_.tag)

    """def addLink(self, name="", link=None):
        """
            Ajout d'un link à l'arbre.
            Paramètres:
            str     name: le nom du link
            RobotLink    link: le link lui meme
            Ne spécifier qu'un des deux paramètres:
            si name est spécifié, le link est créé à partir du nom.
            sinon, le link est directement ajouté.
        """
        self._up_to_date = False

        if name == "":
            if link.name in _links:
                print(link.name, " already in graph !")
            self._links[link.name] = link
        else:
            if name in _links:
                print(name, " already in graph !")
            self._links[name] = RobotLink(name)
    """
    
    def addJoint(self, parentname, childname, joint):
        """
            Ajout d'un joint à l'arbre. La fonction s'occupe de le placer au bon endroit.
            Si les _links parents et enfants n'existent pas, ils seront créés vides.
            Paramètres:
            str     parentname : nom du RobotLink parent
            str     childname : nom du RobotLink enfant
            RobotJoint   joint : joint en question
        """
        self._up_to_date = False

        childLink = None
        parentLink = None
        if parentname in self._links:
            parentLink = self._links[parentname]
        else:
            parentLink = RobotLink(name=parentname)
            self._links[parentname] = parentLink

        if childname in self._links:
            childLink = self._links[childname]
        else:
            childLink = RobotLink(name=childname)
            self._links[childname] = childLink

        childLink.setParent(parentLink, joint)

    def _update(self, ignore_up_to_date=False):
        """
            Met a jour les données internes. Pas besoin de l'appeler.
        """
        if self._up_to_date and not ignore_up_to_date:
            return
        self._is_fully_connected = True
        self.extremities = []
        nb_roots = 0
        for name, link in self._links.items():
            if link.is_extremity:
                self.extremities.append(link.name)
            if link.is_root:
                nb_roots += 1
                self.root = link
        self._is_fully_connected = (nb_roots == 1)
        if not self._is_fully_connected:
            raise Exception("Graph is not fully connected.")
        self._up_to_date = True

    def getLinkTransformsChain(self, link_name, with_extremity_transform=False, axis=None):
        """
            Parametres :
                str                 link_name       nom du RobotLink concerné
                bool                with_extremity_transform    Est-ce qu'on rajoute la transformation du link final?
            Retourne :
                np.array(float)[]   transforms      liste des transformations "fixes" du bras
                                                    ordonnées de la base au RobotLink
                dict(str:data)      joints          liste des joints avec leurs caractéristiques
                                                    "name": nom de l'articulation
                                                    "axis": vecteur unitaire de l'axe
                                                    "transform": fonction qui génère la transformation
                                                    "derivative_transform" : fonction dérivée de la transformation
                                                    "limits": limites physiques de l'articulation
                                                    "velocity_limit": limites de vitesse
                                                    "effort_limit": limites d'effort
                dict(str:data)      symbolic        fonctions symboliques du MGD
                                                    "function" : fonction symbolique
                                                    "transform" : fonction appellable (lambda)
                                                    "var": variables
                                                    "constraints": contraintes
                                                    "all_functions" : liste des transformations dans l'ordre
        """
        if link_name in self._links:
            transforms, joints, symbolic = self._links[link_name].getChain()
            if with_extremity_transform:
                extremity = self._links[link_name].getExtremity(axis)
                symbolic["function"] = symbolic["function"] * \
                    sp.Matrix(extremity)
                transforms.append(extremity)
            symbolic["transform"] = lambdify(
                symbolic["var"], sp.simplify(symbolic["function"]), "numpy")
            return transforms, joints, symbolic
        raise Exception("Did not find the following link : "+str(link_name))

    def display(self):
        self._update()
        if not self._is_fully_connected:
            print("NOT FULLY CONNECTED")
            return
        print("Extremities :")
        for e in self.extremities:
            print(e)
        print("")
        print("Root:"+str(self.root.name)+"\n")
        print("Graph:")
        self.root.displayTree()


if __name__ == "__main__":
    import random
    tree = URDFRobot("resources/rrr_robot.urdf")
    # tree.display()

    a = tree.transforms
    tree.display()
    # print(a)
