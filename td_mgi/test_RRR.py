from control import *
from homogeneous_transform import *

from simulation import *
import unittest
import numpy.testing
import numpy as np
from math import pi

rtol = 1e-6
atol = 1e-6
class TestRRRRobot(unittest.TestCase):
    def setUp(self):
        self.rrr_robot = RRRRobot(urdf="resources/rrr_robot.urdf")
    def test_RRR_operational_limits(self):
        expected = np.array( [[-1.01, 1.01], [-1.01, 1.01],[0.4,1.62]])
        np.testing.assert_allclose(expected, self.rrr_robot.getOperationalDimensionLimits())
        self.rrr_robot = RRRRobot(urdf="resources/rrr_robot.urdf")

    def test_RRR_jac(self):
        joints = np.array([0.,0.,0.])
        expected = np.array([[-1.01,0.,0.,],
                             [ 0.,  0.,  0.,],
                             [ 0.,  0.61,  0.31]])
        np.testing.assert_allclose(expected,self.rrr_robot.computeJacobian(joints), rtol, atol)

    def test_operational_names(self):
        np.testing.assert_equal(["x","y","z"], self.rrr_robot.getOperationalDimensionNames())

    def test_RRR_MGI_analytique_hors_cadre(self):
        target=[0,1.01,0.4]
        expected=0
        returned=self.rrr_robot.analyticalMGI(target)[0]
        np.testing.assert_equal(expected, returned)

    def test_RRR_MGI_analytique_hors_cadre_2(self):
        target=[3,3,3]
        expected=0
        returned=self.rrr_robot.analyticalMGI(target)[0]
        np.testing.assert_equal(expected, returned)
    def test_RRR_jac_2(self):
        joints = np.array([np.pi/2,0.,0.])
        expected = np.array([[0.,0.,0.,],
                             [-1.01,  0.,  0.,],
                             [ 0.,  0.61,  0.31]])
        np.testing.assert_allclose(expected,self.rrr_robot.computeJacobian(joints),rtol, atol)
    def test_flat_arm_transform(self):
        angles = np.array([0.0,0.0,0.0])
        received = self.rrr_robot.getBaseFromToolTransform(angles)
        '''
            Pas de rotations attendues
            Translations attendues:
            base     :   [0,     0,      1.01]
            bras1    :   [0.02,  0.4,    0]
            bras2    :   [-0.02, 0.3,    0]
            endpoint :   [0,     0.31,   0]  < 0.15 (origin) + 0.32/2 (box length / 2)

        '''
        expected = np.array([[1.0, 0.0, 0.0, 0.0],
                     [0.0, 1.0, 0.0, 1.01],
                     [0.0, 0.0, 1.0, 1.01],
                     [0.0, 0.0, 0.0, 1.0]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_1(self):
        dof1 = pi/4
        dof2 = pi/4
        dof3 = pi/4

        angles = np.array([dof1,dof2,dof3])
        received = self.rrr_robot.getBaseFromToolTransform(angles)

        expected = np.array([[ 7.07106781e-01,-1.11022302e-16,7.07106781e-01,-4.32842712e-01],
             [ 7.07106781e-01,1.11022302e-16,-7.07106781e-01,4.32842712e-01],
             [ 0.00000000e+00,1.00000000e+00,2.22044605e-16,1.53213203e+00],
             [ 0.00000000e+00,0.00000000e+00,0.00000000e+00,1.00000000e+00]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_2(self):
        dof1 = pi/4
        dof2 = 0
        dof3 = pi/4
        angles = np.array([dof1,dof2,dof3])
        received = self.rrr_robot.getBaseFromToolTransform(angles)
        expected = np.array([[ 0.70710678,-0.5,0.5,-0.64997475],
             [ 0.70710678,0.5,-0.5,0.64997475],
             [ 0.,0.70710678,0.70710678,1.2292031 ],
             [ 0.,0.,0.,1.]])
        np.testing.assert_allclose(received, expected, rtol, atol)

    def test_transformation_angle_3(self):
        dof1 = 0
        dof2 = pi/4
        dof3 = 0

        angles = np.array([dof1,dof2,dof3])
        received = self.rrr_robot.getBaseFromToolTransform(angles)

        expected = np.array([[ 1.,0.,0.,0.],
             [ 0.,0.70710678,-0.70710678,0.83133514],
             [ 0.,0.70710678,0.70710678,1.44133514],
             [ 0.,0.,0.,1.]])
        np.testing.assert_allclose(received, expected, rtol, atol)

if __name__ == "__main__":
    unittest.main()
