
for i in $(seq 0 999); do
  echo "Début de la pipeline"
  a="$(python3 simulation.py --robot rt --mode jacobianTransposed --auto_id $i --duration 9 --targets targets.csv) "
  echo $a | grep ".csv"
  if [[ $a == *"QBXKZIHFIDIZ"* ]]; then
    echo "Let's test with thresholds now !"
    break
  fi
done
for i in $(seq 0 999); do
  a="$(python3 simulation.py --robot rt --mode jacobianTransposed --auto_th 0.1 --auto_id $i --duration 9 --targets targets.csv) "
  echo $a | grep ".csv"
  if [[ $a == *"QBXKZIHFIDIZ"* ]]; then
    echo "The end."
    exit 1
  fi
done
