
import pandas as pd
import plotly.express as px

import os
import numpy as np
from scipy.stats import pearsonr
import pickle

reload=False
if reload:
    target_file = pd.read_csv("targets.csv")
    nbtargets=  len(target_file["t"])
    nbfiles = len(os.listdir("data/"))
    jacobian_enabled = [False]*nbfiles
    bounds_enabled = [False]*nbfiles
    threshold_enabled = [False]*nbfiles
    file = [None]*nbfiles
    pos_errs = []
    pos_errs = [np.zeros(nbtargets) for nb in range(nbfiles)]
    distance_parcourue = [0.]*nbfiles
    mean_executions = [0.]*nbfiles
    median_executions = [0.]*nbfiles
    nb_errors = [0]*nbfiles
    nb_ticks = [0]*nbfiles
    tick_time = [0]*nbfiles
    i=0

    for filename in os.listdir("./data"):
        print("=====>",filename)
        if "_jac" in filename:
            jacobian_enabled[i]=True
        if "_thr" in filename:
            threshold_enabled[i]=True
        if "_bnd" in filename:
            bounds_enabled[i] = True
        if filename == "targets.csv" or filename == "data_analyzer.py":
            continue
        file[i] = filename
        df = pd.read_csv("data/"+filename)
        execution_times = []
        last_vec = np.array([0.,0.])
        new_vec = np.array([0.,0.])
        current_x = 0
        current_y = 0
        current_z = 0

        last_tick_time = 0
        target_id = 0
        next_target_time = target_file["t"][target_id]
        dest_vec = (target_file["x"][target_id],target_file["y"][target_id])
        for index, row in df.iterrows():
            if row["t"] >= next_target_time and target_id <nbtargets-1:
                pos_errs[i][target_id] = np.sum((new_vec-dest_vec)**2)
                #print("position error at end:",pos_errs[i][target_id])
                target_id += 1
                next_target_time += target_file["t"][target_id]
                dest_vec = (target_file["x"][target_id],
                            target_file["y"][target_id])

            if row["source"] == "measure":
                if "error" in row["name"]:
                    nb_errors[i] = row["value"]
                if row["name"] == "x":
                    current_x = row["value"]
                if row["name"] == "y":
                    current_y = row["value"]
                new_vec = np.array([current_x, current_y])
            if row["name"]=="step duration":
                tick_time[i] = row["value"]
            if row["t"] != last_tick_time:
                distance_parcourue[i] += np.sum((new_vec-last_vec)**2)
                last_vec = new_vec
                nb_ticks[i] += 1
        print("distance parcourue:",distance_parcourue[i])
        print("nombre de ticks:", nb_ticks[i])
        mean_executions[i] =  np.mean(pos_errs[i])
        median_executions[i] = np.median(pos_errs[i])
        print("moyenne des erreurs de position",mean_executions[i] )
        print("médiane des erreurs de position", median_executions[i] )
        print("Nombre de matrices non inversibles", nb_errors[i])
        print("Durée d'un tick:", tick_time[i])
        print("Avec jacobienne:", jacobian_enabled[i])
        print("Avec seuil:", threshold_enabled[i])
        print("Avec bounds:", bounds_enabled[i])
        i+= 1
    pickle.dump((target_file, nbtargets, nbfiles, jacobian_enabled, bounds_enabled,
    threshold_enabled, file, pos_errs, distance_parcourue,
    mean_executions, median_executions, nb_errors, nb_ticks,
    tick_time), open("save.p", "wb"))
else:
    (target_file, nbtargets, nbfiles, jacobian_enabled, bounds_enabled,
    threshold_enabled, file, pos_errs, distance_parcourue,
    mean_executions, median_executions, nb_errors, nb_ticks,
    tick_time) = pickle.load(open("save.p", "rb"))

to_remove = []
for i in range(nbfiles):
    if nb_ticks[i] == 0:
        to_remove.append(i)
(old_target_file, old_nbtargets, old_nbfiles, old_jacobian_enabled, old_bounds_enabled,
old_threshold_enabled, old_file, old_pos_errs, old_distance_parcourue,
old_mean_executions, old_median_executions, old_nb_errors, old_nb_ticks,
old_tick_time)=(target_file.copy(), nbtargets, nbfiles, jacobian_enabled.copy(), bounds_enabled.copy(),
threshold_enabled.copy(), file.copy(), pos_errs.copy(), distance_parcourue.copy(),
mean_executions.copy(), median_executions.copy(), nb_errors.copy(), nb_ticks.copy(),
tick_time.copy())
(target_file, nbtargets, nbfiles, jacobian_enabled, bounds_enabled,
threshold_enabled, file, pos_errs, distance_parcourue,
mean_executions, median_executions, nb_errors, nb_ticks,
tick_time) =([], [], [], [], [],[], [], [], [],[], [], [], [],[])

best_file = ""
best_error = 999.
for i in range(old_nbfiles):
    if i not in to_remove:
        jacobian_enabled.append(old_jacobian_enabled[i])
        bounds_enabled.append(old_bounds_enabled[i])
        threshold_enabled.append(old_threshold_enabled[i])
        file.append(old_file[i])
        pos_errs.append(old_pos_errs[i])
        distance_parcourue.append(old_distance_parcourue[i])
        mean_executions.append(old_mean_executions[i])
        median_executions.append(old_median_executions[i])
        nb_errors.append(old_nb_errors[i])
        nb_ticks.append(old_nb_ticks[i])
        tick_time.append(old_tick_time[i])

nbfiles = len(file)
moyenne_errors = [np.mean(pos_err) for pos_err in pos_errs]
median_errors = [np.median(pos_err) for pos_err in pos_errs]

minimum = 99.
min_median = 99.
for val in moyenne_errors:
    if minimum > val:
        minimum = val
for val in median_errors:
    if min_median > val:
        min_median = val

min_id = moyenne_errors.index(minimum)
min_median_id = median_errors.index(min_median)
print("Minimum d'erreurs :", file[min_id], file[min_median_id], minimum, min_median)
print("distance, erreurs", pearsonr(nb_errors, moyenne_errors))
print("distance, mederreurs", pearsonr(nb_errors, median_errors))
print("erreurs, jac", pearsonr(jacobian_enabled, moyenne_errors))
print("mederreurs, jac", pearsonr(jacobian_enabled, median_errors))
print("erreurs, bn", pearsonr(bounds_enabled, moyenne_errors))
print("mederreurs, bn", pearsonr(bounds_enabled, median_errors))
print("erreurs, th", pearsonr(threshold_enabled, moyenne_errors))
print("mederreurs, th", pearsonr(threshold_enabled, median_errors))
moy_err_with_threshold  = 0
moy_err_with_jac  = 0
moy_err_without_jac  = 0
total_with = 0
total_without = 0
moy_err_without_threshold = 0
for i in range(nbfiles):
    if nb_errors[i]>=50  :
        continue
    if jacobian_enabled[i]:
        total_with += len(pos_errs[i])
        moy_err_with_jac += np.sum(pos_errs[i])
    else:
        total_without += len(pos_errs[i])
        moy_err_without_jac += np.sum(pos_errs[i])

print("Erreur avec jacobienne:", moy_err_with_jac/total_with)
print("Erreur sans jacobienne:", moy_err_without_jac/total_without)
